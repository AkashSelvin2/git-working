﻿var commonConfig =   {
  "Frutiger LT Pro":{
      "45 Light":{  
            "roman":"45 Light",
            "bold":"65 Bold",
            "italic":"46 Light Italic",
            "bolditalic":"66 Bold Italic"
            },
         "46 Light Italic":{  
            "roman":"46 Light Italic",
            "bold":"66 Bold Italic",
            "italic":"45 Light",
            "bolditalic":"65 Bold"
            },
        "47 Light Condensed":{  
            "roman":"47 Light Condensed",
            "bold":"67 Bold Condensed",
            "italic":"48 Light Condensed Italic",
            "bolditalic":"68 Bold Condensed Italic"
            },
      "48 Light Condensed Italic":{  
            "roman":"48 Light Condensed Italic",
            "bold":"68 Bold Condensed Italic",
            "italic":"47 Light Condensed",
            "bolditalic":"67 Bold Condensed"
            },
      "57 Condensed":{  
            "roman":"57 Condensed",
            "bold":"67 Bold Condensed",
            "italic":"58 Condensed Italic",
            "bolditalic":"68 Bold Condensed Italic"
            },
      "58 Condensed Italic":{  
            "roman":"58 Condensed Italic",
            "bold":"68 Bold Condensed Italic",
            "italic":"57 Condensed",
            "bolditalic":"67 Bold Condensed"
            },
      "67 Bold Condensed":{  
            "roman":"67 Bold Condensed",
            "bold":"57 Condensed",
            "italic":"68 Bold Condensed Italic",
            "bolditalic":"58 Condensed Italic"
            },
      "68 Bold Condensed Italic":{  
            "roman":"68 Bold Condensed Italic",
            "bold":"58 Condensed Italic",
            "italic":"67 Bold Condensed",
            "bolditalic":"57 Condensed"
            }
  },
  "Helvetica Neue LT Std":{
        "77 Bold Condensed":{  
            "roman":"77 Bold Condensed",
            "bold":"57 Condensed",
            "italic":"77 Bold Condensed Oblique",
            "bolditalic":"57 Condensed Oblique"
            },
        "75 Bold":{  
            "roman":"75 Bold",
            "bold":"55 Roman",
            "italic":"76 Bold Italic",
            "bolditalic":"56 Italic"
            },
        "55 Roman":{  
            "roman":"55 Roman",
            "bold":"75 Bold",
            "italic":"56 Italic",
            "bolditalic":"76 Bold Italic"
            },
        "76 Bold Italic":{  
            "roman":"76 Bold Italic",
            "bold":"56 Italic",
            "italic":"75 Bold",
            "bolditalic":"55 Roman"
            },
        "57 Condensed":{  
            "roman":"57 Condensed",
            "bold":"77 Bold Condensed",
            "italic":"57 Condensed Oblique",
            "bolditalic":"77 Bold Condensed Oblique"
            },
        "56 Italic":{  
            "roman":"56 Italic",
            "bold":"76 Bold Italic",
            "italic":"55 Roman",
            "bolditalic":"75 Bold"
            },
       "57 Condensed Oblique":{  
            "roman":"57 Condensed Oblique",
            "bold":"77 Bold Condensed Oblique",
            "italic":"57 Condensed",
            "bolditalic":"77 Bold Condensed"
            },
        "77 Bold Condensed Oblique":{  
            "roman":"77 Bold Condensed Oblique",
            "bold":"57 Condensed Oblique",
            "italic":"77 Bold Condensed",
            "bolditalic":"57 Condensed"
            },
       "45 Light":{  
            "roman":"45 Light",
            "bold":"75 Bold",
            "italic":"46 Light Italic",
            "bolditalic":"76 Bold Italic"
            },
         "46 Light Italic":{  
            "roman":"46 Light Italic",
            "bold":"76 Bold Italic",
            "italic":"45 Light",
            "bolditalic":"75 Bold"
            },
       "47 Light Condensed":{  
            "roman":"47 Light Condensed",
            "bold":"77 Bold Condensed",
            "italic":"47 Light Condensed Oblique",
            "bolditalic":"77 Bold Condensed Oblique"
            },
        "47 Light Condensed Oblique":{  
            "roman":"47 Light Condensed",
            "bold":"77 Bold Condensed Oblique",
            "italic":"47 Light Condensed",
            "bolditalic":"77 Bold Condensed"
            },
		"65 Medium":{  
            "roman":"65 Medium",
            "bold":"75 Bold",
            "italic":"66 Medium Italic",
            "bolditalic":"76 Bold Italic"
            },
        "66 Medium Italic":{  
            "roman":"66 Medium Italic",
            "bold":"76 Bold Italic",
            "italic":"65 Medium",
            "bolditalic":"75 Bold"
            },
        "37 Thin Condensed":{  
            "roman":"37 Thin Condensed",
            "bold":"47 Light Condensed",
            "italic":"47 Light Condensed Oblique",
            "bolditalic":"47 Light Condensed Oblique"
            },
        "35 Thin":{  
            "roman":"35 Thin",
            "bold":"45 Light",
            "italic":"35 Thin Italic",
            "bolditalic":"46 Light Italic"
            },
        "35 Thin Italic":{  
            "roman":"35 Thin Italic",
            "bold":"46 Light Italic",
            "italic":"35 Thin",
            "bolditalic":"45 Light"
            },
         "53 Extended":{  
            "roman":"53 Extended",
            "bold":"73 Bold Extended",
            "italic":"53 Extended Oblique",
            "bolditalic":"73 Bold Extended Oblique"
            },
        "73 Bold Extended":{  
            "roman":"73 Bold Extended",
            "bold":"53 Extended",
            "italic":"73 Bold Extended Oblique",
            "bolditalic":"53 Extended Oblique"
            },
           "53 Extended Oblique":{  
            "roman":"53 Extended Oblique",
            "bold":"73 Bold Extended Oblique",
            "italic":"53 Extended",
            "bolditalic":"73 Bold Extended"
            },
        "73 Bold Extended Oblique":{  
            "roman":"73 Bold Extended Oblique",
            "bold":"53 Extended Oblique",
            "italic":"73 Bold Extended",
            "bolditalic":"53 Extended"
            },
        "67 Medium Condensed":{  
            "roman":"67 Medium Condensed",
            "bold":"87 Heavy Condensed",
            "italic":"67 Medium Condensed Oblique",
            "bolditalic":"87 Heavy Condensed Oblique"
            },
        "87 Heavy Condensed":{  
            "roman":"87 Heavy Condensed",
            "bold":"67 Medium Condensed",
            "italic":"87 Heavy Condensed Oblique",
            "bolditalic":"67 Medium Condensed Oblique"
            },
        "67 Medium Condensed Oblique":{  
            "roman":"67 Medium Condensed Oblique",
            "bold":"87 Heavy Condensed Oblique",
            "italic":"67 Medium Condensed",
            "bolditalic":"87 Heavy Condensed"
            },
        "87 Heavy Condensed Oblique":{  
            "roman":"87 Heavy Condensed Oblique",
            "bold":"67 Medium Condensed Oblique",
            "italic":"87 Heavy Condensed",
            "bolditalic":"67 Medium Condensed"
            },
        "83 Heavy Extended":{  
            "roman":"83 Heavy Extended",
            "bold":"93 Black Extended",
            "italic":"83 Heavy Extended Oblique",
            "bolditalic":"93 Black Extended Oblique"
            },
        "93 Black Extended":{  
              "roman":"93 Black Extended",
              "bold":"83 Heavy Extended",
              "italic":"93 Black Extended Oblique",
              "bolditalic":"83 Heavy Extended Oblique"
              },
          "83 Heavy Extended Oblique":{  
              "roman":"83 Heavy Extended Oblique",
              "bold":"93 Black Extended Oblique",
              "italic":"83 Heavy Extended",
              "bolditalic":"93 Black Extended"
              },
           "93 Black Extended Oblique":{  
                "roman":"93 Black Extended Oblique",
                "bold":"83 Heavy Extended Oblique",
                "italic":"93 Black Extended",
                "bolditalic":"83 Heavy Extended"
              },
  }, 
  "Book Antiqua":{
            "Bold":{  
              "roman":"Bold",
              "bold":"Regular",
              "italic":"Bold Italic",
              "bolditalic":"Italic"
              },
            "Bold Italic":{  
                "roman":"Bold Italic",
                "bold":"Italic",
                "italic":"Bold",
                "bolditalic":"Regular"
                  },
              "Regular":{  
              "roman":"Regular",
              "bold":"Bold",
              "italic":"Italic",
              "bolditalic":"Bold Italic"
              },
          "Italic":{  
              "roman":"Italic",
              "bold":"Bold Italic",
              "italic":"Regular",
              "bolditalic":"Bold"
              }
  },
  "Meta Book Lf":{
            "Italic":{  
              "roman":"Italic",
              "bold":{
                    "fontFamily":"MetaBoldLFC",
                    "fontVariant":"Italic"
                    },
              "italic":"Roman",
              "bolditalic":{
                    "fontFamily":"MetaBoldLFC",
                    "fontVariant":"Regular"
                    }
              },
            "Roman":{  
                "roman":"Roman",
                "bold":{
                        "fontFamily":"MetaBoldLFC",
                        "fontVariant":"Regular"
                        },
                "italic":"Italic",
                "bolditalic":{
                        "fontFamily":"MetaBoldLFC",
                        "fontVariant":"Italic"
                        }
              }
  },
  "MetaBoldLFC":{
            "Italic":{  
              "roman":"Italic",
              "bold":{
                    "fontFamily":"Meta Book Lf",
                    "fontVariant":"Italic"
                    },
              "italic":"Regular",
              "bolditalic":{
                    "fontFamily":"Meta Book Lf",
                    "fontVariant":"Roman"
                    }
              },
            "Regular":{  
                "roman":"Regular",
                "bold":{
                    "fontFamily":"Meta Book Lf",
                    "fontVariant":"Roman"
                    },
                "italic":"Italic",
                "bolditalic":{
                    "fontFamily":"Meta Book Lf",
                    "fontVariant":"Italic"
                    }
              }
  },
  "Bookerly":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }				  
  },
  "Ruge Boogie":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold":{  
                  "roman":"null",
                  "bold":"Regular",
                  "italic":"null",
                  "bolditalic":"null"
                  },
            "Italic":{  
                  "roman":"null",
                  "bold":"null",
                  "italic":"Regular",
                  "bolditalic":"null"
                  },
            "Bold Italic":{  
                  "roman":"null",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"Regular"
                  }
  },
  "Helvetica LT Std":{
            "Bold Condensed":{  
              "roman":"Bold Condensed",
              "bold":"Black Condensed",
              "italic":"Bold Condensed Oblique",
              "bolditalic":"Black Condensed Oblique"
              },
            "Condensed":{  
              "roman":"Condensed",
              "bold":"Bold Condensed",
              "italic":"Condensed Oblique",
              "bolditalic":"Bold Condensed Oblique"
              },
			"Condensed Oblique":{  
              "roman":"Condensed Oblique",
              "bold":"Bold Condensed Oblique",
              "italic":"Condensed",
              "bolditalic":"Bold Condensed"
              },
          "Bold Condensed Oblique":{  
              "roman":"Bold Condensed Oblique",
              "bold":"Condensed Oblique",
              "italic":"Bold Condensed",
              "bolditalic":"Condensed"
              },
            "Roman":{  
              "roman":"Roman",
              "bold":"Bold",
              "italic":"Oblique",
              "bolditalic":"Bold Oblique"
              },
            "Bold":{  
              "roman":"Bold",
              "bold":"Black",
              "italic":"Bold Oblique",
              "bolditalic":"Black Oblique"
              },
            "Oblique":{  
              "roman":"Oblique",
              "bold":"Bold Oblique",
              "italic":"Roman",
              "bolditalic":"Bold"
              },
            "Bold Oblique":{  
              "roman":"Bold Oblique",
              "bold":"Black Oblique",
              "italic":"Bold",
              "bolditalic":"Black"
              },
		  "Light":{  
              "roman":"Light",
              "bold":"Roman",
              "italic":"Light Oblique",
              "bolditalic":"Oblique"
              },
		  "Light Oblique":{  
              "roman":"Light Oblique",
              "bold":"Oblique",
              "italic":"Light",
              "bolditalic":"Roman"
              },
          "Black":{  
              "roman":"Black",
              "bold":"Bold",
              "italic":"Black Oblique",
              "bolditalic":"Bold Oblique"
              },
          "Black Oblique":{  
              "roman":"Black Oblique",
              "bold":"Bold Oblique",
              "italic":"Black",
              "bolditalic":"Bold"
              }
  },
  "Helvetica":{
            "Bold Condensed":{  
              "roman":"Bold Condensed",
              "bold":"Black Condensed",
              "italic":"Bold Condensed Oblique",
              "bolditalic":"Black Condensed Oblique"
              }
  },    
  "Calibri":{
            "Bold":{  
              "roman":"Bold",
              "bold":"Regular",
              "italic":"Bold Italic",
              "bolditalic":"Italic"
              },
          "Regular":{  
              "roman":"Regular",
              "bold":"Bold",
              "italic":"Italic",
              "bolditalic":"Bold Italic"
              },
			"Bold Italic":{  
              "roman":"Bold Italic",
              "bold":"Italic",
              "italic":"Bold",
              "bolditalic":"Regular"
              },
			"Italic":{  
              "roman":"Italic",
              "bold":"Bold Italic",
              "italic":"Regular",
              "bolditalic":"Bold"
              },
           "Light":{  
              "roman":"Light",
              "bold":"Bold",
              "italic":"Light Italic",
              "bolditalic":"Bold Italic"
              },
          "Light Italic":{  
              "roman":"Light Italic",
              "bold":"Bold Italic",
              "italic":"Light",
              "bolditalic":"Bold"
              }
  },
  "Myriad Pro":{
            "Light":{  
              "roman":"Light",
              "bold":"Semibold",
              "italic":"Light Italic",
              "bolditalic":"Semibold Italic"
              },
          "Light Italic":{  
              "roman":"Light Italic",
              "bold":"Semibold Italic",
              "italic":"Light",
              "bolditalic":"Semibold"
              },
          "Semibold":{  
              "roman":"Semibold",
              "bold":"Bold",
              "italic":"Semibold Italic",
              "bolditalic":"Bold"
              },
          "Semibold Italic":{  
              "roman":"Semibold Italic",
              "bold":"Bold Italic",
              "italic":"Semibold",
              "bolditalic":"Bold"
              },
			"Regular":{  
              "roman":"Regular",
              "bold":"Bold",
              "italic":"Italic",
              "bolditalic":"Bold Italic"
              },
			"Bold Italic":{  
              "roman":"Bold Italic",
              "bold":"Italic",
              "italic":"Bold",
              "bolditalic":"Regular"
              },
			"Italic":{  
              "roman":"Italic",
              "bold":"Bold Italic",
              "italic":"Regular",
              "bolditalic":"Bold"
              },
            "Bold":{  
              "roman":"Bold",
              "bold":"Regular",
              "italic":"Bold Italic",
              "bolditalic":"Italic"
              },
          "Condensed":{  
                  "roman":"Condensed",
                  "bold":"Bold Condensed",
                  "italic":"Condensed Italic",
                  "bolditalic":"Bold Condensed Italic"
                  },
              "Bold Condensed":{  
                  "roman":"Bold Condensed",
                  "bold":"Condensed",
                  "italic":"Bold Condensed Italic",
                  "bolditalic":"Condensed Italic"
                  },
              "Condensed Italic":{  
                  "roman":"Condensed Italic",
                  "bold":"Bold Condensed Italic",
                  "italic":"Condensed",
                  "bolditalic":"Bold Condensed"
                  },
               "Bold Condensed Italic":{  
                  "roman":"Bold Condensed Italic",
                  "bold":"Condensed Italic",
                  "italic":"Bold Condensed",
                  "bolditalic":"Condensed"
                  }
  },
  "FSAlbert":{
            "Light":{  
              "roman":"Light",
              "bold":"Bold",
              "italic":"Light Italic",
              "bolditalic":"Bold Italic"
              },
          "Light Italic":{  
              "roman":"Light Italic",
              "bold":"Bold Italic",
              "italic":"Light",
              "bolditalic":"Bold"
              },
			"Regular":{  
              "roman":"Regular",
              "bold":"Bold",
              "italic":"Italic",
              "bolditalic":"Bold Italic"
              },
			"Bold Italic":{  
              "roman":"Bold Italic",
              "bold":"Italic",
              "italic":"Bold",
              "bolditalic":"Regular"
              },
			"Italic":{  
              "roman":"Italic",
              "bold":"Bold Italic",
              "italic":"Regular",
              "bolditalic":"Bold"
              },
            "Bold":{  
              "roman":"Bold",
              "bold":"Regular",
              "italic":"Bold Italic",
              "bolditalic":"Italic"
              }
  },
  "MetaBookLFC":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  }
  },
  "Gill Sans":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Condensed":{  
                  "roman":"Condensed",
                  "bold":"Bold Condensed",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold Condensed":{  
                  "roman":"Bold Condensed",
                  "bold":"Condensed",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  }
  },
  "Caecilia LT Std":{
            "75 Bold":{  
                  "roman":"75 Bold",
                  "bold":"55 Roman",
                  "italic":"76 Bold Italic",
                  "bolditalic":"56 Italic"
                  },
            "56 Italic":{  
                  "roman":"56 Italic",
                  "bold":"76 Bold Italic",
                  "italic":"55 Roman",
                  "bolditalic":"75 Bold"
                  },
            "55 Roman":{  
                  "roman":"55 Roman",
                  "bold":"75 Bold",
                  "italic":"56 Italic",
                  "bolditalic":"76 Bold Italic"
                  },
			"76 Bold Italic":{  
                  "roman":"76 Bold Italic",
                  "bold":"56 Italic",
                  "italic":"75 Bold",
                  "bolditalic":"55 Roman"
                  }
  },
  "Vectora LT Std":{
            "45 Light":{  
                  "roman":"45 Light",
                  "bold":"75 Bold",
                  "italic":"46 Light Italic",
                  "bolditalic":"76 Bold Italic"
                  },
            "46 Light Italic":{  
                  "roman":"46 Light Italic",
                  "bold":"76 Bold Italic",
                  "italic":"45 Light",
                  "bolditalic":"75 Bold"
                  },
            "55 Roman":{  
                  "roman":"55 Roman",
                  "bold":"75 Bold",
                  "italic":"56 Italic",
                  "bolditalic":"76 Bold Italic"
                  },
            "56 Italic":{  
                  "roman":"56 Italic",
                  "bold":"76 Bold Italic",
                  "italic":"55 Roman",
                  "bolditalic":"75 Bold"
                  },
            "75 Bold":{  
                  "roman":"75 Bold",
                  "bold":"55 Roman",
                  "italic":"76 Bold Italic",
                  "bolditalic":"56 Italic"
                  },
			"76 Bold Italic":{  
                  "roman":"76 Bold Italic",
                  "bold":"56 Italic",
                  "italic":"75 Bold",
                  "bolditalic":"55 Roman"
                  }
  },
 "Facit":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Semibold":{  
                  "roman":"Semibold",
                  "bold":"Bold",
                  "italic":"Semibold Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Light Italic":{  
                  "roman":"Light Italic",
                  "bold":"Semibold Italic",
                  "italic":"Light",
                  "bolditalic":"Semibold"
                  },
            "Light":{  
                  "roman":"Light",
                  "bold":"Semibold",
                  "italic":"Light Italic",
                  "bolditalic":"Semibold Italic"
                  },
			"Semibold Italic":{  
                  "roman":"Semibold Italic",
                  "bold":"Light Italic",
                  "italic":"Semibold",
                  "bolditalic":"Light"
                  }
          },
  "Janson Text LT Std":{
            "75 Bold":{  
                  "roman":"75 Bold",
                  "bold":"55 Roman",
                  "italic":"76 Bold Italic",
                  "bolditalic":"56 Italic"
                  },
            "56 Italic":{  
                  "roman":"56 Italic",
                  "bold":"76 Bold Italic",
                  "italic":"55 Roman",
                  "bolditalic":"75 Bold"
                  },
            "55 Roman":{  
                  "roman":"55 Roman",
                  "bold":"75 Bold",
                  "italic":"56 Italic",
                  "bolditalic":"76 Bold Italic"
                  },
			"76 Bold Italic":{  
                  "roman":"76 Bold Italic",
                  "bold":"56 Italic",
                  "italic":"75 Bold",
                  "bolditalic":"55 Roman"
                  }
  },
  "Cambria":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
  },
  "Zurich BT":{
            "Condensed":{  
                  "roman":"Condensed",
                  "bold":"Bold Condensed",
                  "italic":"Condensed Italic",
                  "bolditalic":"Bold Condensed Italic"
                  },
              "Bold Condensed":{  
                  "roman":"Bold Condensed",
                  "bold":"Condensed",
                  "italic":"Bold Condensed Italic",
                  "bolditalic":"Condensed Italic"
                  },
              "Condensed Italic":{  
                  "roman":"Condensed Italic",
                  "bold":"Bold Condensed Italic",
                  "italic":"Condensed",
                  "bolditalic":"Bold Condensed"
                  },
               "Bold Condensed Italic":{  
                  "roman":"Bold Condensed Italic",
                  "bold":"Condensed Italic",
                  "italic":"Bold Condensed",
                  "bolditalic":"Condensed"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
  },
  "MetaPro":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Normal",
                  "italic":"BoldItalic",
                  "bolditalic":"NormalItalic"
                  },
            "NormalItalic":{  
                  "roman":"NormalItalic",
                  "bold":"BoldItalic",
                  "italic":"Normal",
                  "bolditalic":"Bold"
                  },
            "Normal":{  
                  "roman":"Normal",
                  "bold":"Bold",
                  "italic":"NormalItalic",
                  "bolditalic":"BoldItalic"
                  },
            "BoldItalic":{  
                  "roman":"BoldItalic",
                  "bold":"NormalItalic",
                  "italic":"Bold",
                  "bolditalic":"Normal"
                  },
              "Book":{  
                  "roman":"Book",
                  "bold":"Bold",
                  "italic":"BookItalic",
                  "bolditalic":"BoldItalic"
                  },
              "BookItalic":{  
                  "roman":"BookItalic",
                  "bold":"BoldItalic",
                  "italic":"Book",
                  "bolditalic":"Bold"
                  },
              "Black":{  
                  "roman":"Black",
                  "bold":"Medium",
                  "italic":"BlackItalic",
                  "bolditalic":"NormalItalic"
                  },
            "MediumItalic":{  
                  "roman":"MediumItalic",
                  "bold":"BlackItalic",
                  "italic":"Medium",
                  "bolditalic":"Black"
                  },
            "Medium":{  
                  "roman":"Medium",
                  "bold":"Black",
                  "italic":"MediumItalic",
                  "bolditalic":"BlackItalic"
                  },
            "BlackItalic":{  
                  "roman":"BlackItalic",
                  "bold":"MediumItalic",
                  "italic":"Black",
                  "bolditalic":"Medium"
                  }
  },
  "Meta Pro Normal":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":{
                        "fontFamily":"Meta Pro Medium",
                        "fontVariant":"Regular"
                        },
                  "italic":"Italic",
                  "bolditalic":{
                        "fontFamily":"Meta Pro Medium",
                        "fontVariant":"Italic"
                        }
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":{
                        "fontFamily":"Meta Pro Medium",
                        "fontVariant":"Italic"
                        },
                  "italic":"Regular",
                  "bolditalic":{
                        "fontFamily":"Meta Pro Medium",
                        "fontVariant":"Regular"
                        }
                  }
  },
  "Meta Pro Medium":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":{
                        "fontFamily":"Meta Pro Bold",
                        "fontVariant":"Regular"
                        },
                  "italic":"Italic",
                  "bolditalic":{
                        "fontFamily":"Meta Pro Bold",
                        "fontVariant":"Italic"
                        }
                  },
               "Italic":{  
                  "roman":"Italic",
                  "bold":{
                        "fontFamily":"Meta Pro Bold",
                        "fontVariant":"Italic"
                        },
                  "italic":"Regular",
                  "bolditalic":{
                        "fontFamily":"Meta Pro Bold",
                        "fontVariant":"Regular"
                        }
                  } 
},
  "Times New Roman MT Std":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Condensed":{  
                  "roman":"Condensed",
                  "bold":"Bold Condensed",
                  "italic":"Condensed Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold Condensed":{  
                  "roman":"Bold Condensed",
                  "bold":"Condensed",
                  "italic":"Bold Italic",
                  "bolditalic":"Condensed Italic"
                  },
              "Condensed Italic":{  
                  "roman":"Condensed Italic",
                  "bold":"Bold Italic",
                  "italic":"Condensed",
                  "bolditalic":"Bold Condensed"
                  }
  },
"Times New Roman ps":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }				  
  },
"Times New Roman PS":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }				  
  },
  "CenturyExpd BT":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }				  
  },
"Grotesque MT Std":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"null"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"null",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"null",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Bold Extended":{  
                  "roman":"Bold Extended",
                  "bold":"Regular",
                  "italic":"null",
                  "bolditalic":"Italic"
                  }				  
  },
 "Minion Phonetic Pro":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Semibold",
                  "italic":"Italic",
                  "bolditalic":"Semibold Italic"
                  },
            "Semibold":{  
                  "roman":"Semibold",
                  "bold":"Regular",
                  "italic":"Semibold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Semibold Italic",
                  "italic":"Regular",
                  "bolditalic":"Semibold"
                  },
            "Semibold Italic":{  
                  "roman":"Semibold Italic",
                  "bold":"Italic",
                  "italic":"Semibold",
                  "bolditalic":"Regular"
                  }				  
  },
 "ITC Leawood Std":{
            "Book":{  
                  "roman":"Book",
                  "bold":"Bold",
                  "italic":"Book Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Book",
                  "italic":"Bold Italic",
                  "bolditalic":"Book Italic"
                  },
            "Book Italic":{  
                  "roman":"Book Italic",
                  "bold":"Bold Italic",
                  "italic":"Book",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Book Italic",
                  "italic":"Bold",
                  "bolditalic":"Book"
                  },
              "Medium":{  
                  "roman":"Medium",
                  "bold":"Black",
                  "italic":"Medium Italic",
                  "bolditalic":"Black Italic"
                  },
            "Black":{  
                  "roman":"Black",
                  "bold":"Medium",
                  "italic":"Black Italic",
                  "bolditalic":"Medium Italic"
                  },
            "Medium Italic":{  
                  "roman":"Medium Italic",
                  "bold":"Black Italic",
                  "italic":"Medium",
                  "bolditalic":"Black"
                  },
            "Black Italic":{  
                  "roman":"Black Italic",
                  "bold":"Medium Italic",
                  "italic":"Black",
                  "bolditalic":"Medium"
                  }
  },
  "Goudy":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Old Style",
                  "italic":"Bold Italic",
                  "bolditalic":"Oldstyle Italic"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Oldstyle Italic",
                  "italic":"Bold",
                  "bolditalic":"Old Style"
                  },
              "Old Style":{  
                  "roman":"Old Style",
                  "bold":"Bold",
                  "italic":"Oldstyle Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Oldstyle Italic":{  
                  "roman":"Oldstyle Italic",
                  "bold":"Bold Italic",
                  "italic":"Old Style",
                  "bolditalic":"Bold"
                  }
  },
  "Goudy (TT)":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Old Style",
                  "italic":"Bold Italic",
                  "bolditalic":"Oldstyle Italic"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Oldstyle Italic",
                  "italic":"Bold",
                  "bolditalic":"Old Style"
                  },
              "Old Style":{  
                  "roman":"Old Style",
                  "bold":"Bold",
                  "italic":"Oldstyle Italic",
                  "bolditalic":"Semibold Italic"
                  },
              "Oldstyle Italic":{  
                  "roman":"Oldstyle Italic",
                  "bold":"Bold Italic",
                  "italic":"Old Style",
                  "bolditalic":"Bold"
                  }           
  },
  "Goudy Old Style BT":{
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  }
  },
  "Trade Gothic LT Std":{
            "Bold No. 2":{  
                  "roman":"Bold No. 2",
                  "bold":"Regular",
                  "italic":"Bold No. 2 Oblique",
                  "bolditalic":"Oblique"
                  },
              "Bold No. 2 Oblique":{  
                  "roman":"Bold No. 2 Oblique",
                  "bold":"Oblique",
                  "italic":"Bold No. 2",
                  "bolditalic":"Regular"
                  },
			"Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Oblique",
                  "bolditalic":"Bold Oblique"
                  },
			"Light":{  
                  "roman":"Light",
                  "bold":"Bold",
                  "italic":"Oblique",
                  "bolditalic":"Bold Oblique"
                  },
			"Oblique":{  
                  "roman":"Oblique",
                  "bold":"Bold Oblique",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Oblique",
                  "bolditalic":"Oblique"
                  },
			"Bold Extended":{  
                  "roman":"Bold Extended",
                  "bold":"Extended",
                  "italic":"Bold Oblique",
                  "bolditalic":"Oblique"
                  },
              "Extended":{  
                  "roman":"Extended",
                  "bold":"Bold Extended",
                  "italic":"Oblique",
                  "bolditalic":"Bold Oblique"
                  },
              "Condensed No. 18":{  
                  "roman":"Condensed No. 18",
                  "bold":"Bold Condensed No. 20",
                  "italic":"Condensed No. 18 Oblique",
                  "bolditalic":"Bold Condensed No. 20 Oblique"
                  },
            "Bold Condensed No. 20":{  
                  "roman":"Bold Condensed No. 20",
                  "bold":"Condensed No. 18",
                  "italic":"Bold Condensed No. 20 Oblique",
                  "bolditalic":"Condensed No. 18 Oblique"
                  },
            "Condensed No. 18 Oblique":{  
                  "roman":"Condensed No. 18 Oblique",
                  "bold":"Bold Condensed No. 20 Oblique",
                  "italic":"Condensed No. 18",
                  "bolditalic":"Bold Condensed No. 20"
                  },
            "Bold Condensed No. 20 Oblique":{  
                  "roman":"Bold Condensed No. 20 Oblique",
                  "bold":"Condensed No. 18 Oblique",
                  "italic":"Bold Condensed No. 20",
                  "bolditalic":"Condensed No. 18"
                  }
  },
  "Garamond (OTF)":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                "roman":"Bold Italic",
                "bold":"Italic",
                "italic":"Bold",
                "bolditalic":"Regular"
			    }
  },
  "Minion Pro":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Subhead":{  
                  "roman":"Subhead",
                  "bold":"Bold Subhead",
                  "italic":"Italic Subhead",
                  "bolditalic":"Bold Italic Subhead"
                  },
            "Semibold":{  
                  "roman":"Semibold",
                  "bold":"Bold",
                  "italic":"Semibold Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Semibold Italic":{  
                  "roman":"Semibold Italic",
                  "bold":"Bold Italic",
                  "italic":"Semibold",
                  "bolditalic":"Bold"
                  },
              "Bold Cond Caption":{  
                  "roman":"Bold Cond Caption",
                  "bold":"Cond Caption",
                  "italic":"Bold Cond Italic Caption",
                  "bolditalic":"Cond Italic Caption"
                  },
            "Cond Caption":{  
                  "roman":"Cond Caption",
                  "bold":"Bold Cond Caption",
                  "italic":"Cond Italic Caption",
                  "bolditalic":"Bold Cond Italic Caption"
                  },
			"Cond Italic Caption":{  
                  "roman":"Cond Italic Caption",
                  "bold":"Bold Cond Italic Caption",
                  "italic":"Cond Caption",
                  "bolditalic":"Bold Cond Caption"
                  },
              "Bold Cond Italic Caption":{  
                  "roman":"Bold Cond Italic Caption",
                  "bold":"Cond Italic Caption",
                  "italic":"Bold Cond Caption",
                  "bolditalic":"Cond Caption"
                  },
              "Cond Display":{  
                  "roman":"Cond Display",
                  "bold":"Bold Cond Display",
                  "italic":"Cond Italic Display",
                  "bolditalic":"Bold Cond Italic Display"
                  },
            "Bold Cond Display":{  
                  "roman":"Bold Cond Display",
                  "bold":"Cond Display",
                  "italic":"Bold Cond Italic Display",
                  "bolditalic":"Cond Italic Display"
                  },
            "Cond Italic Display":{  
                  "roman":"Cond Italic Display",
                  "bold":"Bold Cond Italic Display",
                  "italic":"Cond Display",
                  "bolditalic":"Bold Cond Display"
                  },
            "Bold Cond Italic Display":{  
                  "roman":"Bold Cond Italic Display",
                  "bold":"Cond Italic Display",
                  "italic":"Bold Cond Display",
                  "bolditalic":"Cond Display"
                  },
              "Cond":{  
                  "roman":"Cond",
                  "bold":"Bold Cond",
                  "italic":"Cond Italic",
                  "bolditalic":"Bold Cond Italic"
                  },
            "Bold Cond":{  
                  "roman":"Bold Cond",
                  "bold":"Cond",
                  "italic":"Bold Cond Italic",
                  "bolditalic":"Cond Italic"
                  },
            "Cond Italic":{  
                  "roman":"Cond Italic",
                  "bold":"Bold Cond Italic",
                  "italic":"Cond",
                  "bolditalic":"Bold Cond"
                  },
            "Bold Cond Italic":{  
                  "roman":"Bold Cond Italic",
                  "bold":"Cond Italic",
                  "italic":"Bold Cond",
                  "bolditalic":"Cond"
                  },
              "Caption":{  
                  "roman":"Caption",
                  "bold":"Bold Caption",
                  "italic":"Italic Caption",
                  "bolditalic":"Bold Italic Caption"
                  },
            "Bold Caption":{  
                  "roman":"Bold Caption",
                  "bold":"Caption",
                  "italic":"Bold Italic Caption",
                  "bolditalic":"Italic Caption"
                  },
            "Italic Caption":{  
                  "roman":"Italic Caption",
                  "bold":"Bold Italic Caption",
                  "italic":"Caption",
                  "bolditalic":"Bold Caption"
                  },
            "Bold Italic Caption":{  
                  "roman":"Bold Italic Caption",
                  "bold":"Italic Caption",
                  "italic":"Bold Caption",
                  "bolditalic":"Caption"
                  },
              "Display":{  
                  "roman":"Display",
                  "bold":"Bold Display",
                  "italic":"Italic Display",
                  "bolditalic":"Bold Italic Display"
                  },
            "Bold Display":{  
                  "roman":"Bold Display",
                  "bold":"Display",
                  "italic":"Bold Italic Display",
                  "bolditalic":"Italic Display"
                  },
            "Italic Display":{  
                  "roman":"Italic Display",
                  "bold":"Bold Italic Display",
                  "italic":"Display",
                  "bolditalic":"Bold Display"
                  },
            "Bold Italic Display":{  
                  "roman":"Bold Italic Display",
                  "bold":"Italic Display",
                  "italic":"Bold Display",
                  "bolditalic":"Display"
                  }
  },
  "Plantin Std":{
       "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Semibold",
                  "italic":"Light Italic",
                  "bolditalic":"Semibold Italic"
                  },
            "Semibold":{  
                  "roman":"Semibold",
                  "bold":"Bold",
                  "italic":"Semibold Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Light Italic":{  
                  "roman":"Light Italic",
                  "bold":"Semibold Italic",
                  "italic":"Light",
                  "bolditalic":"Semibold"
                  },
              "Semibold Italic":{  
                  "roman":"Semibold Italic",
                  "bold":"Bold Italic",
                  "italic":"Semibold",
                  "bolditalic":"Bold"
                  },
              "Bold Condensed":{  
                  "roman":"Bold Condensed",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  }
  },
  "Scala":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  }
  },
  "Minion":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  }
  },
 "Perpetua Titling MT":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Light",
                  "italic":"null",
                  "bolditalic":"null"
                  },
            "Light":{  
                  "roman":"Light",
                  "bold":"Bold",
                  "italic":"null",
                  "bolditalic":"null"
                  }
  },
  "Arial":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Black",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
               "Narrow":{  
                  "roman":"Narrow",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
               "Black":{  
                  "roman":"Black",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  }
  },
"Stone Serif":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
     "Stone Serif Semi Bold":{
            "Italic":{  
              "roman":"Italic",
              "bold":{
                    "fontFamily":"Stone Serif",
                    "fontVariant":"Bold Italic"
                    },
              "italic":"Regular",
              "bolditalic":{
                    "fontFamily":"Stone Serif",
                    "fontVariant":"Bold"
                    }
              },
            "Regular":{  
                "roman":"Regular",
                "bold":{
                    "fontFamily":"Stone Serif",
                    "fontVariant":"Bold"
                    },
                "italic":"Italic",
                "bolditalic":{
                    "fontFamily":"Stone Serif",
                    "fontVariant":"Bold Italic"
                    }
              }
        },
  "Times New Roman":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
    },
	"Andika":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
    },
	"Optima Tr":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Normal",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Normal":{  
                  "roman":"Normal",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Normal",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Normal"
                  }
    },
"Chaparral Pro":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Light Italic":{  
                  "roman":"Light Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  }
    },
  "Adobe Garamond Pro":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
			"Semibold":{  
                  "roman":"Semibold",
                  "bold":"Bold",
                  "italic":"Semibold Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Semibold Italic":{  
                  "roman":"Semibold Italic",
                  "bold":"Bold Italic",
                  "italic":"Semibold",
                  "bolditalic":"Bold"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
  },
  "Adobe Garamond":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                "roman":"Bold Italic",
                "bold":"Italic",
                "italic":"Bold",
                "bolditalic":"Regular"
                  },				  
            "Semibold":{  
                "roman":"Semibold",
                "bold":"Bold",
                "italic":"Semibold Italic",
                "bolditalic":"Bold Italic"
                },				  
            "Semibold Italic":{  
                "roman":"Semibold Italic",
                "bold":"Bold Italic",
                "italic":"Semibold",
                "bolditalic":"Bold"
                 },
              "Italic Exp":{  
                  "roman":"Italic Exp",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Titling Capitals":{  
                  "roman":"Titling Capitals",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  }
  },
  "Garamond":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                "roman":"Bold Italic",
                "bold":"Italic",
                "italic":"Bold",
                "bolditalic":"Regular"
			    }
  },
  "Univers LT Std":{
        "65 Bold":{  
            "roman":"65 Bold",
            "bold":"55 Roman",
            "italic":"65 Bold Oblique",
            "bolditalic":"55 Oblique"
            },
        "55 Roman":{  
            "roman":"55 Roman",
            "bold":"65 Bold",
            "italic":"55 Oblique",
            "bolditalic":"65 Bold Oblique"
            },
        "55 Oblique":{  
            "roman":"56 Oblique",
            "bold":"65 Bold Oblique",
            "italic":"55 Roman",
            "bolditalic":"65 Bold"
            },
        "65 Bold Oblique":{  
            "roman":"65 Bold Oblique",
            "bold":"55 Oblique",
            "italic":"65 Bold",
            "bolditalic":"55 Roman"
            },
        "57 Condensed":{  
            "roman":"57 Condensed",
            "bold":"67 Bold Condensed",
            "italic":"57 Condensed Oblique",
            "bolditalic":"67 Bold Condensed Oblique"
            },
        "67 Bold Condensed":{  
            "roman":"67 Bold Condensed",
            "bold":"57 Condensed",
            "italic":"67 Bold Condensed Oblique",
            "bolditalic":"57 Condensed Oblique"
            },
       "57 Condensed Oblique":{  
            "roman":"57 Condensed Oblique",
            "bold":"67 Bold Condensed Oblique",
            "italic":"57 Condensed",
            "bolditalic":"67 Bold Condensed"
            },
        "67 Bold Condensed Oblique":{  
            "roman":"67 Bold Condensed Oblique",
            "bold":"57 Condensed Oblique",
            "italic":"67 Bold Condensed",
            "bolditalic":"57 Condensed"
            },
       "45 Light":{  
            "roman":"45 Light",
            "bold":"65 Bold",
            "italic":"45 Light Oblique",
            "bolditalic":"65 Bold Oblique"
            },
         "45 Light Oblique":{  
            "roman":"45 Light Oblique",
            "bold":"65 Bold Oblique",
            "italic":"45 Light",
            "bolditalic":"65 Bold"
            },
       "47 Light Condensed":{  
            "roman":"47 Light Condensed",
            "bold":"67 Bold Condensed",
            "italic":"47 Light Condensed Oblique",
            "bolditalic":"67 Bold Condensed Oblique"
            },
        "47 Light Condensed Oblique":{  
            "roman":"47 Light Condensed",
            "bold":"67 Bold Condensed Oblique",
            "italic":"47 Light Condensed",
            "bolditalic":"67 Bold Condensed"
            },
        "39 Thin Ultra Condensed":{  
            "roman":"39 Thin Ultra Condensed",
            "bold":"47 Light Condensed",
            "italic":"47 Light Condensed Oblique",
            "bolditalic":"67 Bold Condensed Oblique"
            },
        "49 Light Ultra Condensed":{  
            "roman":"49 Light Ultra Condensed",
            "bold":"47 Light Condensed",
            "italic":"47 Light Condensed Oblique",
            "bolditalic":"67 Bold Condensed Oblique"
            },
        "59 Ultra Condensed":{  
            "roman":"59 Ultra Condensed",
            "bold":"47 Light Condensed",
            "italic":"47 Light Condensed Oblique",
            "bolditalic":"67 Bold Condensed Oblique"
            },
        "75 Black":{  
              "roman":"75 Black",
              "bold":"85 Extra Black",
              "italic":"75 Black Oblique",
              "bolditalic":"85 Extra Black Oblique"
              },
        "85 Extra Black":{  
              "roman":"85 Extra Black",
              "bold":"75 Black",
              "italic":"85 Extra Black Oblique",
              "bolditalic":"75 Black Oblique"
              },
        "75 Black Oblique":{  
              "roman":"75 Black Oblique",
              "bold":"85 Extra Black Oblique",
              "italic":"75 Black",
              "bolditalic":"85 Extra Black"
              },
        "85 Extra Black Oblique":{  
              "roman":"85 Extra Black Oblique",
              "bold":"75 Black Oblique",
              "italic":"85 Extra Black",
              "bolditalic":"75 Black"
              },
          "53 Extended":{  
              "roman":"53 Extended",
              "bold":"63 Bold Extended",
              "italic":"53 Extended Oblique",
              "bolditalic":"63 Bold Extended Oblique"
              },
        "63 Bold Extended":{  
              "roman":"63 Bold Extended",
              "bold":"53 Extended",
              "italic":"63 Bold Extended Oblique",
              "bolditalic":"53 Extended Oblique"
              },
        "53 Extended Oblique":{  
              "roman":"53 Extended Oblique",
              "bold":"63 Bold Extended Oblique",
              "italic":"53 Extended",
              "bolditalic":"63 Bold Extended"
              },
        "63 Bold Extended Oblique":{  
              "roman":"63 Bold Extended Oblique",
              "bold":"53 Extended Oblique",
              "italic":"63 Bold Extended",
              "bolditalic":"53 Extended"
              },
          "73 Black Extended":{  
              "roman":"73 Black Extended",
              "bold":"93 Extra Black Extended",
              "italic":"73 Black Extended Oblique",
              "bolditalic":"93 Extra Black Extended Oblique"
              },
        "93 Extra Black Extended":{  
              "roman":"93 Extra Black Extended",
              "bold":"73 Black Extended",
              "italic":"93 Extra Black Extended Oblique",
              "bolditalic":"73 Black Extended Oblique"
              },
        "73 Black Extended Oblique":{  
              "roman":"73 Black Extended Oblique",
              "bold":"93 Extra Black Extended Oblique",
              "italic":"73 Black Extended",
              "bolditalic":"93 Extra Black Extended"
              },
        "93 Extra Black Extended Oblique":{  
              "roman":"93 Extra Black Extended Oblique",
              "bold":"73 Black Extended Oblique",
              "italic":"93 Extra Black Extended",
              "bolditalic":"73 Black Extended"
              }
    }, 
  "Univers 55":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":{
                    "fontFamily":"Univers 45 Light",
                    "fontVariant":"Bold"
                    },
                  "italic":"Italic",
                  "bolditalic":{
                    "fontFamily":"Univers 45 Light",
                    "fontVariant":"Bold Italic"
                    }
                },
            "Italic":{  
                  "roman":"Italic",
                  "bold":{
                    "fontFamily":"Univers 45 Light",
                    "fontVariant":"Bold Italic"
                    },
                  "italic":"Regular",
                  "bolditalic":{
                    "fontFamily":"Univers 45 Light",
                    "fontVariant":"Bold"
                    }
			},
        "Bold":{  
                  "roman":{
                    "fontFamily":"Univers 45 Light",
                    "fontVariant":"Bold"
                    },
                  "bold":"Regular",
                  "italic":{
                    "fontFamily":"Univers 45 Light",
                    "fontVariant":"Bold Italic"
                    },
                  "bolditalic":"Italic"
			},
         "Bold Italic":{  
                  "roman":{
                    "fontFamily":"Univers 45 Light",
                    "fontVariant":"Bold Italic"
                    },
                  "bold":"Italic",
                  "italic":{
                    "fontFamily":"Univers 45 Light",
                    "fontVariant":"Bold"
                    },
                  "bolditalic":"Regular"
			}
  },
  "Univers 45 Light":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
  },
 "Times":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
  },
  "DIN-Bold":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  }
  },
  "DIN Next LT Pro":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Medium",
                  "italic":"Light Italic",
                  "bolditalic":"Medium Italic"
                 },
             "Light Italic":{  
                  "roman":"Light Italic",
                  "bold":"Medium Italic",
                  "italic":"Light",
                  "bolditalic":"Medium"
                 },
			"Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Medium Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Medium Italic":{  
                  "roman":"Medium Italic",
                  "bold":"Bold Italic",
                  "italic":"Medium",
                  "bolditalic":"Bold"
                  }
            
  },
  "Gill Sans MT":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
  },
  "Stone Sans ITC Pro":{
            "Semi Bold":{  
                  "roman":"Semi Bold",
                  "bold":"Bold",
                  "italic":"Semi Bold Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Semi Bold Italic":{  
                  "roman":"Semi Bold Italic",
                  "bold":"Bold Italic",
                  "italic":"Semi Bold",
                  "bolditalic":"Bold Italic"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Medium",
                  "italic":"Bold Italic",
                  "bolditalic":"Medium Italic"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Semi Bold Italic",
                  "italic":"Semi Bold",
                  "bolditalic":"Bold"
                  },
			"Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Medium Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Medium Italic":{  
                  "roman":"Medium Italic",
                  "bold":"Bold Italic",
                  "italic":"Medium",
                  "bolditalic":"Bold"
                  }
  },
"Stone Sans":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
        "Stone Sans Semi Bold":{
            "Italic":{  
              "roman":"Italic",
              "bold":{
                    "fontFamily":"Stone Sans",
                    "fontVariant":"Bold Italic"
                    },
              "italic":"Regular",
              "bolditalic":{
                    "fontFamily":"Stone Sans",
                    "fontVariant":"Bold"
                    }
              },
            "Regular":{  
                "roman":"Regular",
                "bold":{
                    "fontFamily":"Stone Sans",
                    "fontVariant":"Bold"
                    },
                "italic":"Italic",
                "bolditalic":{
                    "fontFamily":"Stone Sans",
                    "fontVariant":"Bold Italic"
                    }
              },
          "Bold":{  
                  "roman":{
                    "fontFamily":"Stone Sans",
                    "fontVariant":"Bold"
                    },
                  "bold":"Regular",
                  "italic":{
                    "fontFamily":"Stone Sans",
                    "fontVariant":"Bold Italic"
                    },
                  "bolditalic":"Italic"
			},
         "Bold Italic":{  
                  "roman":{
                    "fontFamily":"Stone Sans",
                    "fontVariant":"Bold Italic"
                    },
                  "bold":"Italic",
                  "italic":{
                    "fontFamily":"Univers 45 Light",
                    "fontVariant":"Bold"
                    },
                  "bolditalic":"Regular"
			}
        },
  "Frutiger LT Std":{
            "57 Condensed":{  
                  "roman":"57 Condensed",
                  "bold":"67 Bold Condensed",
                  "italic":"58 Condensed Italic",
                  "bolditalic":"68 Bold Condensed Italic"
                  },
            "67 Bold Condensed":{  
                  "roman":"67 Bold Condensed",
                  "bold":"57 Condensed",
                  "italic":"68 Bold Condensed Italic",
                  "bolditalic":"58 Condensed Italic"
                  },
              "58 Condensed Italic":{  
                  "roman":"58 Condensed Italic",
                  "bold":"68 Bold Condensed Italic",
                  "italic":"57 Condensed",
                  "bolditalic":"67 Bold Condensed"
                  },
              "68 Bold Condensed Italic":{  
                  "roman":"68 Bold Condensed Italic",
                  "bold":"58 Condensed Italic",
                  "italic":"67 Bold Condensed",
                  "bolditalic":"57 Condensed"
                  },
            "87 Extra Black Condensed":{  
                  "roman":"87 Extra Black Condensed",
                  "bold":"77 Black Condensed",
                  "italic":"null",
                  "bolditalic":"null"
                  },
               "55 Roman":{  
                  "roman":"55 Roman",
                  "bold":"65 Bold",
                  "italic":"56 Italic",
                  "bolditalic":"66 Bold Italic"
                  },
               "56 Italic":{  
                  "roman":"56 Italic",
                  "bold":"66 Bold Italic",
                  "italic":"55 Roman",
                  "bolditalic":"65 Bold"
                  },
            "65 Bold":{  
                  "roman":"65 Bold",
                  "bold":"75 Black",
                  "italic":"66 Bold Italic",
                  "bolditalic":"76 Black Italic"
                  },
              "66 Bold Italic":{  
                  "roman":"66 Bold Italic",
                  "bold":"56 Italic",
                  "italic":"65 Bold",
                  "bolditalic":"55 Roman"
                  },
            "46 Light Italic":{  
                  "roman":"46 Light Italic",
                  "bold":"65 Bold",
                  "italic":"46 Light Italic",
                  "bolditalic":"66 Bold Italic"
                  },
            "45 Light":{  
                  "roman":"45 Light",
                  "bold":"65 Bold",
                  "italic":"46 Light Italic",
                  "bolditalic":"66 Bold Italic"
                  },
              "47 Light Condensed":{  
                  "roman":"47 Light Condensed",
                  "bold":"67 Bold Condensed",
                  "italic":"48 Light Condensed Italic",
                  "bolditalic":"68 Bold Condensed Italic"
                  },
               "48 Light Condensed Italic":{  
                  "roman":"48 Light Condensed Italic",
                  "bold":"68 Bold Condensed Italic",
                  "italic":"47 Light Condensed",
                  "bolditalic":"67 Bold Condensed"
                  },
              "77 Black Condensed":{  
                  "roman":"77 Black Condensed",
                  "bold":"87 Extra Black Condensed",
                  "italic":"null",
                  "bolditalic":"null"
                  },
              "75 Black":{  
                  "roman":"75 Black",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  },
              "95 Ultra Black":{  
                  "roman":"95 Ultra Black",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  }
  },
  "ZapfCalligr BT":{
        "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
  },

  "MetaHeadlineOT":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"null",
                  "bolditalic":"null"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Black",
                  "italic":"null",
                  "bolditalic":"null"
                  },
               "Light":{  
                  "roman":"Light",
                  "bold":"Regular",
                  "italic":"null",
                  "bolditalic":"null"
                  },
              "Black":{  
                  "roman":"Black",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  }
  },
 "Duplicate Slab":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Regular Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Regular Italic":{  
                  "roman":"Regular Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Regular Italic"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Regular Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Medium",
                  "italic":"Light Italic",
                  "bolditalic":"Medium Italic"
                  },
              "Medium":{  
                  "roman":"Medium",
                  "bold":"Heavy",
                  "italic":"Medium Italic",
                  "bolditalic":"Heavy Italic"
                  },
              "Light Italic":{  
                  "roman":"Light Italic",
                  "bold":"Medium Italic",
                  "italic":"Light",
                  "bolditalic":"Medium"
                  },
              "Medium Italic":{  
                  "roman":"Medium Italic",
                  "bold":"Heavy Italic",
                  "italic":"Medium",
                  "bolditalic":"Heavy"
                  }
  },
  "Duplicate Ionic":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Regular Italic"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Regular Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
               "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Regular Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Regular Italic":{  
                  "roman":"Regular Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  }
  },
  "FreeSans":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Medium",
                  "italic":"BoldOblique",
                  "bolditalic":"Oblique"
                  },
              "Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Oblique",
                  "bolditalic":"BoldOblique"
                  },
              "Oblique":{  
                  "roman":"Oblique",
                  "bold":"BoldOblique",
                  "italic":"Medium",
                  "bolditalic":"Bold"
                  },
              "BoldOblique":{  
                  "roman":"BoldOblique",
                  "bold":"Oblique",
                  "italic":"Bold",
                  "bolditalic":"Medium"
                  }
  },
  "Gotham":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Book",
                  "italic":"ItalicBold",
                  "bolditalic":"Book Italic"
                  },
              "Book":{  
                  "roman":"Book",
                  "bold":"Bold",
                  "italic":"Book Italic",
                  "bolditalic":"ItalicBold"
                  },
              "Book Italic":{  
                  "roman":"Book Italic",
                  "bold":"ItalicBold",
                  "italic":"Book",
                  "bolditalic":"Bold"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Bold",
                  "italic":"Light Italic",
                  "bolditalic":"ItalicBold"
              },
          "Light Italic":{  
                  "roman":"Light Italic",
                  "bold":"ItalicBold",
                  "italic":"Light",
                  "bolditalic":"Bold"
              },
          "ItalicBold":{  
                  "roman":"ItalicBold",
                  "bold":"Book Italic",
                  "italic":"Bold",
                  "bolditalic":"Book"
              }
  },
  "ITC Berkeley Oldstyle Std":{
			  "Bold":{  
                  "roman":"Bold",
                  "bold":"Black",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Book":{  
                  "roman":"Book",
                  "bold":"Bold",
                  "italic":"Book Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Book Italic":{  
                  "roman":"Book Italic",
                  "bold":"Bold Italic",
                  "italic":"Book",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
				  "roman":"Bold Italic",
            	  "bold":"Book Italic",
            	  "italic":"Bold",
            	  "bolditalic":"Book"
              },
              "Black":{  
                  "roman":"Black",
                  "bold":"Bold",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
              }
  },
  "Modernica":{
            "Thin":{  
                  "roman":"Thin",
                  "bold":"Regular",
                  "italic":"Thin Italic",
                  "bolditalic":"Italic"
                  },
			"Thin Italic":{  
                  "roman":"Thin Italic",
                  "bold":"Italic",
                  "italic":"Thin",
                  "bolditalic":"Regular"
                  },
			"Light":{  
                  "roman":"Light",
                  "bold":"Medium",
                  "italic":"Light Italic",
                  "bolditalic":"Medium Italic"
                 },
			"Light Italic":{  
                  "roman":"Light Italic",
                  "bold":"Medium Italic",
                  "italic":"Light",
                  "bolditalic":"Medium"
                 },
			"Book":{  
                  "roman":"Book",
                  "bold":"Bold",
                  "italic":"Book Italic",
                  "bolditalic":"ItalicBold"
                  },
            "Book Italic":{  
                  "roman":"Book Italic",
                  "bold":"ItalicBold",
                  "italic":"Book",
                  "bolditalic":"Bold"
                  },	 
			"Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Italic":{  
				"roman":"Italic",
				"bold":"Bold Italic",
				"italic":"Regular",
				"bolditalic":"Bold"
				  },
			"Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Medium Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Medium Italic":{  
                  "roman":"Medium Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
			"Bold Italic":{  
				  "roman":"Bold Italic",
            	  "bold":"Italic",
            	  "italic":"Bold",
            	  "bolditalic":"Regular"
              },
			"Heavy":{  
                  "roman":"Heavy",
                  "bold":"Black",
                  "italic":"Heavy Italic",
                  "bolditalic":"Black Italic"
                  },
			"Heavy Italic":{  
                  "roman":"Heavy Italic",
                  "bold":"Black Italic",
                  "italic":"Heavy",
                  "bolditalic":"Black"
                  },
			"Black":{  
                  "roman":"Black",
                  "bold":"Heavy",
                  "italic":"Black Italic",
                  "bolditalic":"Heavy Italic"
              },
			"Black Italic":{  
                  "roman":"Black Italic",
                  "bold":"Heavy Italic",
                  "italic":"Black",
                  "bolditalic":"Heavy"
              }
    },
  "Palatino":{
             "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  },
              "Light Italic":{  
                  "roman":"Light talic",
                  "bold":"Bold Italic",
                  "italic":"Light",
                  "bolditalic":"Bold"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Bold",
                  "italic":"Light Italic",
                  "bolditalic":"Bold Italic"
                  }
  },
  "Palatino LT Std":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
  },
  "Photina MT Std":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Semibold":{  
                  "roman":"Semibold",
                  "bold":"Ultra Bold",
                  "italic":"Semibold Italic",
                  "bolditalic":"Ultra Bold Italic"
                  },
              "Ultra Bold":{  
                  "roman":"Ultra Bold",
                  "bold":"Semibold",
                  "italic":"Ultra Bold Italic",
                  "bolditalic":"Semibold Italic"
                  },
              "Semibold Italic":{  
                  "roman":"Semibold Italic",
                  "bold":"Ultra Bold Italic",
                  "italic":"Semibold",
                  "bolditalic":"Ultra Bold"
                  },
              "Ultra Bold Italic":{  
                  "roman":"Ultra Bold Italic",
                  "bold":"Semibold Italic",
                  "italic":"Ultra Bold",
                  "bolditalic":"Semibold"
                  }
  },
  "Goudy Old Style":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"null",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"null",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
               "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"null"
                  }
  },
  "ITC Giovanni Std":{
            "Book":{  
                  "roman":"Book",
                  "bold":"Bold",
                  "italic":"Book Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Book Italic":{  
                  "roman":"Book Italic",
                  "bold":"Bold Italic",
                  "italic":"Book",
                  "bolditalic":"Bold"
                  },
               "Bold":{  
                  "roman":"Bold",
                  "bold":"Book",
                  "italic":"Bold Italic",
                  "bolditalic":"Book Italic"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Book Italic",
                  "italic":"Bold",
                  "bolditalic":"Book"
                  }
  },
  "Bernhard Modern Std":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
  },
  
  "Avenir":{
              "Roman":{  
                  "roman":"Roman",
                  "bold":"Black",
                  "italic":"Oblique",
                  "bolditalic":"Black Oblique"
                  },
            "Black":{  
                  "roman":"Black",
                  "bold":"Roman",
                  "italic":"Black Oblique",
                  "bolditalic":"Oblique"
                  },
            "Oblique":{  
                  "roman":"Oblique",
                  "bold":"Black Oblique",
                  "italic":"Roman",
                  "bolditalic":"Black"
                  },
            "Black Oblique":{  
                  "roman":"Black Oblique",
                  "bold":"Oblique",
                  "italic":"Black",
                  "bolditalic":"Roman"
                  },
              "Medium":{  
                  "roman":"Medium",
                  "bold":"Black",
                  "italic":"Medium Oblique",
                  "bolditalic":"Black Oblique"
                  },
              "Medium Oblique":{  
                  "roman":"Medium Oblique",
                  "bold":"Black Oblique",
                  "italic":"Medium",
                  "bolditalic":"Black"
                  },
              "Book":{  
                  "roman":"Roman",
                  "bold":"Heavy",
                  "italic":"Book Oblique",
                  "bolditalic":"Heavy Oblique"
                  },
            "Heavy":{  
                  "roman":"Heavy",
                  "bold":"Book",
                  "italic":"Heavy Oblique",
                  "bolditalic":"Book Oblique"
                  },
            "Book Oblique":{  
                  "roman":"Book Oblique",
                  "bold":"Heavy Oblique",
                  "italic":"Book",
                  "bolditalic":"Heavy"
                  },
            "Heavy Oblique":{  
                  "roman":"Heavy Oblique",
                  "bold":"Book Oblique",
                  "italic":"Heavy",
                  "bolditalic":"Book"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Medium",
                  "italic":"Light Oblique",
                  "bolditalic":"Medium Oblique"
                  },
              "Light Oblique":{  
                  "roman":"Light Oblique",
                  "bold":"Medium Oblique",
                  "italic":"Light",
                  "bolditalic":"Medium"
                  }
  },
  "Avenir LT Std":{
            "35 Light":{  
                  "roman":"35 Light",
                  "bold":"65 Medium",
                  "italic":"35 Light Oblique",
                  "bolditalic":"65 Medium Oblique"
                  },
              "35 Light Oblique":{  
                  "roman":"35 Light Oblique",
                  "bold":"65 Medium Oblique",
                  "italic":"35 Light",
                  "bolditalic":"65 Medium"
                  },
            "55 Roman":{  
                  "roman":"55 Roman",
                  "bold":"95 Black",
                  "italic":"55 Oblique",
                  "bolditalic":"95 Black Oblique"
                  },
              "55 Oblique":{  
                  "roman":"55 Oblique",
                  "bold":"95 Black Oblique",
                  "italic":"55 Roman",
                  "bolditalic":"95 Black"
                  },
            "65 Medium":{  
                  "roman":"65 Medium",
                  "bold":"95 Black",
                  "italic":"65 Medium Oblique",
                  "bolditalic":"95 Black Oblique"
                  },
               "65 Medium Oblique":{  
                  "roman":"65 Medium Oblique",
                  "bold":"95 Black Oblique",
                  "italic":"65 Medium",
                  "bolditalic":"95 Black"
                  },
			"95 Black":{  
                  "roman":"95 Black",
                  "bold":"55 Roman",
                  "italic":"95 Black Oblique",
                  "bolditalic":"55 Oblique"
                  },
              "95 Black Oblique":{  
                  "roman":"95 Black Oblique",
                  "bold":"55 Oblique",
                  "italic":"95 Black Oblique",
                  "bolditalic":"55 Roman"
                  },
            "85 Heavy":{  
                  "roman":"85 Heavy",
                  "bold":"95 Black",
                  "italic":"85 Heavy Oblique",
                  "bolditalic":"95 Black Oblique"
                  },
              "85 Oblique":{  
                  "roman":"85 Heavy Oblique",
                  "bold":"95 Black Oblique",
                  "italic":"85 Heavy",
                  "bolditalic":"95 Black"
                  },
               "45 Book":{  
                  "roman":"45 Book",
                  "bold":"85 Heavy",
                  "italic":"45 Book Oblique",
                  "bolditalic":"85 Heavy Oblique"
                  },
               "45 Book Oblique":{  
                  "roman":"45 Book Oblique",
                  "bold":"85 Heavy Oblique",
                  "italic":"45 Book",
                  "bolditalic":"85 Heavy"
                  }
  },
  "ITC Garamond Std":{
            "Light":{  
                  "roman":"Light",
                  "bold":"Bold",
                  "italic":"Light Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Light",
                  "italic":"Bold Italic",
                  "bolditalic":"Light Italic"
                  },
            "Light Italic":{  
                  "roman":"Light Italic",
                  "bold":"Bold Italic",
                  "italic":"Light",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Light Italic",
                  "italic":"Bold",
                  "bolditalic":"Light"
                  },
              "Light Condensed":{  
                  "roman":"Light Condensed",
                  "bold":"Bold",
                  "italic":"Light Italic",
                  "bolditalic":"Bold Italic"
                  }
  },
  "Bliss 2":{
            "ExtraLight":{  
                  "roman":"ExtraLight",
                  "bold":"ExtraBold",
                  "italic":"null",
                  "bolditalic":"null"
                  },
              "ExtraBold":{  
                  "roman":"ExtraBold",
                  "bold":"ExtraLight",
                  "italic":"null",
                  "bolditalic":"null"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Bold",
                  "italic":"null",
                  "bolditalic":"null"
                  },
              "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"null"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"null",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"null",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  }
  },
"Bliss Pro":{
            "ExtraLight":{  
                  "roman":"ExtraLight",
                  "bold":"ExtraBold",
                  "italic":"ExtraLight Italic",
                  "bolditalic":"ExtraBold Italic"
                  },
              "ExtraBold":{  
                  "roman":"ExtraBold",
                  "bold":"ExtraLight",
                  "italic":"ExtraBold Italic",
                  "bolditalic":"ExtraLight Italic"
                  },
              "ExtraLight Italic":{  
                  "roman":"ExtraLight Italic",
                  "bold":"ExtraBold Italic",
                  "italic":"ExtraLight",
                  "bolditalic":"ExtraBold"
                  },
              "ExtraBold Italic":{  
                  "roman":"ExtraBold Italic",
                  "bold":"ExtraLight Italic",
                  "italic":"ExtraBold",
                  "bolditalic":"ExtraLight"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Bold",
                  "italic":"Light Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Light Italic":{  
                  "roman":"Light Italic",
                  "bold":"Bold Italic",
                  "italic":"Light",
                  "bolditalic":"Bold"
                  },
              "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
  },
  "Brill":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
  },
"Adobe Caslon Pro":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Semibold":{  
                  "roman":"Semibold",
                  "bold":"Bold",
                  "italic":"Semibold Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Semibold Italic":{  
                  "roman":"Semibold Italic",
                  "bold":"Bold Italic",
                  "italic":"Semibold",
                  "bolditalic":"Bold"
                  }
  },
"Dante MT Std":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Medium Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Medium Italic":{  
                  "roman":"Medium Italic",
                  "bold":"Bold Italic",
                  "italic":"Medium",
                  "bolditalic":"Bold"
                  }
  },
"Dax":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"BoldItalic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"BoldItalic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"BoldItalic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "BoldItalic":{  
                  "roman":"BoldItalic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"MediumItalic",
                  "bolditalic":"BoldItalic"
                  },
              "MediumItalic":{  
                  "roman":"MediumItalic",
                  "bold":"BoldItalic",
                  "italic":"Medium",
                  "bolditalic":"Bold"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Medium",
                  "italic":"MediumItalic",
                  "bolditalic":"Bold"
                  }
  },
  "Raleway":{
            "Light":{  
                  "roman":"Light",
                  "bold":"Medium",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "SemiBold":{  
                  "roman":"SemiBold",
                  "bold":"ExtraBold",
                  "italic":"SemiBold Italic",
                  "bolditalic":"ExtraBold Italic"
                  },
              "SemiBold Italic":{  
                  "roman":"SemiBold Italic",
                  "bold":"ExtraBold Italic",
                  "italic":"SemiBold",
                  "bolditalic":"ExtraBold"
                  },
              "ExtraBold":{  
                  "roman":"ExtraBold",
                  "bold":"SemiBold",
                  "italic":"ExtraBold Italic",
                  "bolditalic":"SemiBold Italic"
                  },
              "ExtraBold Italic":{  
                  "roman":"ExtraBold Italic",
                  "bold":"SemiBold Italic",
                  "italic":"ExtraBold",
                  "bolditalic":"SemiBold"
                  }
  },
  "ClassGarmnd BT":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
  },
"Schneidler BT":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
               "Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Medium Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Medium Italic":{  
                  "roman":"Medium Italic",
                  "bold":"Bold Italic",
                  "italic":"Medium",
                  "bolditalic":"Bold"
                  }
  },
  "ITC New Baskerville":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
  },
"InterFace":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
               "Light":{  
                  "roman":"Light",
                  "bold":"Bold",
                  "italic":"Light Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Light Italic":{  
                  "roman":"Light Italic",
                  "bold":"Bold Italic",
                  "italic":"Light",
                  "bolditalic":"Bold"
                  }
        },
    "Sabon":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
        },
    "Sabon LT Std":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
        },
		"Swis721 BT":{
            "Roman":{
                "roman":"Roman",
                  "bold":{
                    "fontFamily":"Swiss 721 BT",
                    "fontVariant":"Bold"
                        }
                    },
                  "italic":{
                    "fontFamily":"Swiss 721 BT",
                    "fontVariant":"Italic"
                    },
                  "bolditalic":{
                    "fontFamily":"Swiss 721 BT",
                    "fontVariant":"Bold Italic"
                }
            },
        "Swiss 721 BT":{
              "Bold":{  
                  "roman":"Bold",
                  "bold":{
                    "fontFamily":"Swis721 BT",
                    "fontVariant":"Roman"
                    },
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":{
                    "fontFamily":"Swis721 BT",
                    "fontVariant":"Roman"
                    },
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":{
                    "fontFamily":"Swis721 BT",
                    "fontVariant":"Roman"
                    }
                  }
        },
		"Optima":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Oblique",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Oblique"
                  },
              "Oblique":{  
                  "roman":"Oblique",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Oblique",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
        },
  "Walbaum Com":{
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
        },
    "Walbaum":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "EhrhardtMT":{
            "Roman":{  
                  "roman":"Roman",
                   "bold":{
                    "fontFamily":"'EhrhardtMT-SemiBold",
                    "fontVariant":"Semi Bold"
                    },
                  "italic":"RomanItalic",
                  "bolditalic":{
                    "fontFamily":"EhrhardtMT-SemiBold",
                    "fontVariant":"Semi BoldItalic"
                    }
                  },
              "RomanItalic":{  
                  "roman":"RomanItalic",
                  "bold":{
                    "fontFamily":"EhrhardtMT-SemiBold",
                    "fontVariant":"Semi BoldItalic"
                    },
                  "italic":"Roman",
                  "bolditalic":{
                    "fontFamily":"EhrhardtMT-SemiBold",
                    "fontVariant":"Semi Bold"
                    }
			}
        },
    "EhrhardtMT-SemiBold":{
            "Semi Bold":{  
                  "roman":"Semi Bold",
                  "bold":{
                    "fontFamily":"EhrhardtMT",
                    "fontVariant":"Roman"
                    },
                  "italic":"Semi BoldItalic",
                  "bolditalic":{
                    "fontFamily":"EhrhardtMT",
                    "fontVariant":"RomanItalic"
                    }
                  },
              "Semi BoldItalic":{  
                  "roman":"Semi BoldItalic",
                  "bold":{
                    "fontFamily":"EhrhardtMT",
                    "fontVariant":"RomanItalic"
                    },
                  "italic":"Semi Bold",
                  "bolditalic":{
                    "fontFamily":"EhrhardtMT",
                    "fontVariant":"Roman"
                    }
                  }
              },
     "Zurich Condensed BT":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
        },
 "Akzidenz-Grotesk BQ":{
        "Regular":{  
              "roman":"Regular",
              "bold":"Bold",
              "italic":"Italic",
              "bolditalic":"Bold Italic"
              },
          "Bold":{  
              "roman":"Bold",
              "bold":"Regular",
              "italic":"Bold Italic",
              "bolditalic":"Italic"
              },
           "Italic":{  
              "roman":"Italic",
              "bold":"Bold Italic",
              "italic":"Regular",
              "bolditalic":"Bold"
              },
          "Bold Italic":{  
              "roman":"Bold Italic",
              "bold":"Italic",
              "italic":"Bold",
              "bolditalic":"Roman"
              },
          "Medium":{  
              "roman":"Medium",
              "bold":"Bold",
              "italic":"Medium Italic",
              "bolditalic":"Bold Italic"
              },
          "Medium Italic":{  
              "roman":"Medium Italic",
              "bold":"Bold Italic",
              "italic":"Medium",
              "bolditalic":"Bold"
              } 
     },
    "Bliss-Medium":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  }                  
         },
     "BlissExtraBold":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  }                  
         },
     "BlissLight":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  }                  
         },
     "BlissMedium":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  }                  
         },
     "DIN Pro":{
            "Italic":{  
                  "roman":"Italic",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  }                  
         },
     "TradeGothic Bold":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  }                  
         },
     "TradeGothic":{
            "Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Oblique",
                  "bolditalic":"Bold Oblique"
                  },
              "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Oblique",
                  "bolditalic":"Bold Oblique"
                  },
              "Oblique":{  
                  "roman":"Oblique",
                  "bold":"Bold Oblique",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  } ,
              "Bold Oblique":{  
                  "roman":"Bold Oblique",
                  "bold":"Oblique",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Oblique",
                  "bolditalic":"Oblique"
                  }
         },
     "TradeGothic Light":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"null",
                  "italic":"null",
                  "bolditalic":"null"
                  }                  
         },
    "Courier New":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Dante MT":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "ITC Galliard":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
        },
    "Gill Sans MT":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "InterFace DaMa":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Meridien":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Roman",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Roman",
                  "bolditalic":"Bold"
                  },
            "Roman":{  
                  "roman":"Roman",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Roman"
                  }
        },
    "Meta Pro Light":{
            "Italic":{  
                  "roman":"Italic",
                  "bold":{
                        "fontFamily":"Meta Pro Medium",
                        "fontVariant":"Italic"
                        },
                  "italic":"Regular",
                  "bolditalic":{
                        "fontFamily":"Meta Pro Medium",
                        "fontVariant":"Regular"
                        }
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":{
                        "fontFamily":"Meta Pro Medium",
                        "fontVariant":"Regular"
                        },
                  "italic":"Italic",
                  "bolditalic":{
                        "fontFamily":"Meta Pro Medium",
                        "fontVariant":"Italic"
                        }
                  }
        },
     "MetaBlackLF":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"null",
                  "italic":{
                        "fontFamily":"Meta",
                        "fontVariant":"Black Italic"
                        },
                  "bolditalic":"null"
                  }
              },
    "Meta Serif Pro Book":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "ITC Officina Sans Std":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Book",
                  "italic":"Bold Italic",
                  "bolditalic":"Book Italic"
                  },
            "Book Italic":{  
                  "roman":"Book Italic",
                  "bold":"Bold Italic",
                  "italic":"Book",
                  "bolditalic":"Bold"
                  },
            "Book":{  
                  "roman":"Book",
                  "bold":"Bold",
                  "italic":"Book Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Book Italic",
                  "italic":"Bold",
                  "bolditalic":"Book"
                  }
        },
    "ITC Tiepolo Std":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Book",
                  "italic":"Bold Italic",
                  "bolditalic":"Book Italic"
                  },
            "Book Italic":{  
                  "roman":"Book Italic",
                  "bold":"Bold Italic",
                  "italic":"Book",
                  "bolditalic":"Bold"
                  },
            "Book":{  
                  "roman":"Book",
                  "bold":"Bold",
                  "italic":"Book Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Book Italic",
                  "italic":"Bold",
                  "bolditalic":"Book"
                  }
        },
    "Palatino Linotype":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Stone Sans":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Stone Serif":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Rotis Serif Std":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "ZurichLightCondensedBT":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Aparajita":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Bookman Old Style":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Calisto MT":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Candara":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Century Gothic":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Century Schoolbook":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Consolas":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Constantia":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Corbel":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Adobe Devanagari":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Euclid":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Euclid Symbol":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
      "Georgia":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Kokila":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Perpetua":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Rockwell":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Tw Cen MT":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Utsaah":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Verdana":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Trebuchet MS":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Arial Unicode MS":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Adobe Arabic":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Myriad Arabic":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Adobe Hebrew":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Myriad Hebrew":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Angsana New":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "AngsanaUPC":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Browallia New":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "BrowalliaUPC":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Cordia New":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "CordiaUPC":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "DilleniaUPC":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "EucrosiaUPC":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "FreesiaUPC":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "IrisUPC":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "JasmineUPC":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "KodchiangUPC":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "LilyUPC":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
      "Bliss":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
          },
      "DIN-Regular":{
              "Regular":{  
                  "roman":"Regular",
                  "bold":{
                    "fontFamily":"DIN-Bold",
                    "fontVariant":"Bold"
                    },
                  "italic":{
                    "fontFamily":"DIN Next LT Pro",
                    "fontVariant":"Italic"
                    },
                  "bolditalic":{
                    "fontFamily":"DIN Next LT Pro",
                    "fontVariant":"Bold Italic"
                    }
                  }
          },
      "Trajan Pro":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"null",
                  "bolditalic":"null"
                  },
              "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"null",
                  "bolditalic":"null"
                  }
          },
      "Zurich Extended BT":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"null",
                  "bolditalic":"null"
                  },
              "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"null",
                  "bolditalic":"null"
                  }
          },
      "Zurich Extra Condensed BT":{
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"null",
                  "bolditalic":"null"
                  },
              "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"null",
                  "bolditalic":"null"
                  }
          },
    "Schneidler Black BT":{
            "Italic":{  
              "roman":"Italic",
              "bold":"null",
              "italic":"Regular",
              "bolditalic":"null"
              },
            "Regular":{  
                "roman":"Regular",
                "bold":"null",
                "italic":"Italic",
                "bolditalic":"null"
              }
        },
    
    "Stone Sans Semi Bold":{
            "Italic":{  
              "roman":"Italic",
              "bold":"null",
              "italic":"Regular",
              "bolditalic":"null"
              },
            "Regular":{  
                "roman":"Regular",
                "bold":"null",
                "italic":"Italic",
                "bolditalic":"null"
              }
        },
    "Zurich Black BT":{
            "Italic":{  
              "roman":"Italic",
              "bold":"null",
              "italic":"Regular",
              "bolditalic":"null"
              },
            "Regular":{  
                "roman":"Regular",
                "bold":"null",
                "italic":"Italic",
                "bolditalic":"null"
              }
        },
    "Zurich Light BT":{
            "Italic":{  
              "roman":"Italic",
              "bold":"null",
              "italic":"Regular",
              "bolditalic":"null"
              },
            "Regular":{  
                "roman":"Regular",
                "bold":"null",
                "italic":"Italic",
                "bolditalic":"null"
              }
        },
    "Zurich Light Condensed BT":{
            "Italic":{  
              "roman":"Italic",
              "bold":"null",
              "italic":"Regular",
              "bolditalic":"null"
              },
            "Regular":{  
                "roman":"Regular",
                "bold":"null",
                "italic":"Italic",
                "bolditalic":"null"
              }
        },
    "Meta Book Lf Caps":{
            "Italic":{  
              "roman":"Italic",
              "bold":"null",
              "italic":"Regular",
              "bolditalic":"null"
              },
            "Regular":{  
                "roman":"Regular",
                "bold":"null",
                "italic":"Italic",
                "bolditalic":"null"
              }
        },
     "Geogrotesque":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
               "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
               "Medium":{  
                  "roman":"Medium",
                  "bold":"SemiBold",
                  "italic":"Medium Italic",
                  "bolditalic":"SemiBold Italic"
                  },
              "SemiBold":{  
                  "roman":"SemiBold",
                  "bold":"Medium",
                  "italic":"SemiBold Italic",
                  "bolditalic":"Medium Italic"
                  },
              "Medium Italic":{  
                  "roman":"Medium Italic",
                  "bold":"SemiBold Italic",
                  "italic":"Medium",
                  "bolditalic":"SemiBold"
                  },
              "SemiBold Italic":{  
                  "roman":"SemiBold Italic",
                  "bold":"Medium Italic",
                  "italic":"SemiBold",
                  "bolditalic":"Medium"
                  }
         },
     "Bembo":{
            "Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"BoldItalic"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Medium",
                  "italic":"BoldItalic",
                  "bolditalic":"Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"BoldItalic",
                  "italic":"Medium",
                  "bolditalic":"Bold"
                  },
			"BoldItalic":{  
                  "roman":"BoldItalic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Medium"
                  }
        },
    "FreeSerif":{
            "Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"BoldItalic"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Medium",
                  "italic":"BoldItalic",
                  "bolditalic":"Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"BoldItalic",
                  "italic":"Medium",
                  "bolditalic":"Bold"
                  },
			"BoldItalic":{  
                  "roman":"BoldItalic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Medium"
                  }
        },
    "Lucida Bright":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Demibold",
                  "italic":"Italic",
                  "bolditalic":"Demibold Italic"
                  },
			"Demibold":{  
                  "roman":"Demibold",
                  "bold":"Regular",
                  "italic":"Demibold Italic",
                  "bolditalic":"Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Demibold Italic",
                  "italic":"Regular",
                  "bolditalic":"Demibold"
                  },
			"Demibold Italic":{  
                  "roman":"Demibold Italic",
                  "bold":"Italic",
                  "italic":"Demibold",
                  "bolditalic":"Regular"
                  }
        },
    "Lucida Fax":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Demibold",
                  "italic":"Italic",
                  "bolditalic":"Demibold Italic"
                  },
			"Demibold":{  
                  "roman":"Demibold",
                  "bold":"Regular",
                  "italic":"Demibold Italic",
                  "bolditalic":"Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"Demibold Italic",
                  "italic":"Regular",
                  "bolditalic":"Demibold"
                  },
			"Demibold Italic":{  
                  "roman":"Demibold Italic",
                  "bold":"Italic",
                  "italic":"Demibold",
                  "bolditalic":"Regular"
                  }
        },
    "Lucida Sans Typewriter":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Oblique",
                  "bolditalic":"Bold Oblique"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Oblique",
                  "bolditalic":"Oblique"
                  },
			"Oblique":{  
                  "roman":"Oblique",
                  "bold":"Bold Oblique",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
			"Bold Oblique":{  
                  "roman":"Bold Oblique",
                  "bold":"Oblique",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
        },
    "Letter Gothic Std":{
            "Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Slanted",
                  "bolditalic":"Bold Slanted"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Medium",
                  "italic":"Bold Slanted",
                  "bolditalic":"Slanted"
                  },
			"Slanted":{  
                  "roman":"Slanted",
                  "bold":"Bold Slanted",
                  "italic":"Medium",
                  "bolditalic":"Bold"
                  },
			"Bold Slanted":{  
                  "roman":"Bold Slanted",
                  "bold":"Slanted",
                  "italic":"Bold",
                  "bolditalic":"Medium"
                  }
        },
    "DIN OT":{
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"null",
                  "bolditalic":"null"
                  },
			"Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"null",
                  "bolditalic":"null"
                  }
        },
    "DINPro":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Medium Italic",
                  "bolditalic":"Bold Italic"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Medium Italic"
                  },
              "Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Medium Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Regular",
                  "italic":"Medium Italic",
                  "bolditalic":"Bold Italic"
                  }
        },
    "ITC Avant Garde Gothic Std":{
            "Book Condensed":{  
                  "roman":"Book Condensed",
                  "bold":"Bold Condensed",
                  "italic":"null",
                  "bolditalic":"null"
                  },
			"Bold Condensed":{  
                  "roman":"Bold Condensed",
                  "bold":"Book Condensed",
                  "italic":"null",
                  "bolditalic":"null"
                  },
			"Demi Condensed":{  
                  "roman":"Demi Condensed",
                  "bold":"Bold Condensed",
                  "italic":"null",
                  "bolditalic":"null"
                  },
			"Extra Light":{  
                  "roman":"Extra Light",
                  "bold":"Demi Condensed",
                  "italic":"null",
                  "bolditalic":"null"
                  }
        },
    "AvantGarde-Book":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":{
                        "fontFamily":"ITC Avant Garde Gothic LT Demi",
                        "fontVariant":"Regular"
                        },
                  "italic":{
                        "fontFamily":"AvantGarde-BookOblique",
                        "fontVariant":"Regular"
                        },
                  "bolditalic":{
                        "fontFamily":"ITC Avant Garde Gothic LT Demi",
                        "fontVariant":"Regular"
                        }
                  }
        },
    
    "Meta Lf Caps":{
            "Medium":{  
                  "roman":"Medium",
                  "bold":"Bold",
                  "italic":"Medium Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Medium",
                  "italic":"Bold Italic",
                  "bolditalic":"Medium Italic"
                  },
              "Medium Italic":{  
                  "roman":"Medium Italic",
                  "bold":"Bold Italic",
                  "italic":"Medium",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Medium Italic",
                  "italic":"Bold",
                  "bolditalic":"Medium"
                  }
        },
    "Meta Lf":{
            "Roman Bold":{  
              "roman":"Roman Bold",
              "bold":"null",
              "italic":"Bold Italic",
              "bolditalic":"null"
              },
            "Bold Italic":{  
                "roman":"Bold Italic",
                "bold":"null",
                "italic":"Roman Bold",
                "bolditalic":"null"
              }
        },
     "ITC Stone Sans":{
            "Italic":{  
              "roman":"Italic",
              "bold":"Bold Italic",
              "italic":"null",
              "bolditalic":"null"
              },
            "Bold Italic":{  
                "roman":"Bold Italic",
                "bold":"Italic",
                "italic":"null",
                "bolditalic":"null"
              }
        },
    "Futura Md BT":{
            "Medium Italic":{  
              "roman":"Medium Italic",
              "bold":"Bold Italic",
              "italic":"Medium",
              "bolditalic":"Bold"
              },
            "Medium":{  
                "roman":"Medium",
                "bold":"Bold",
                "italic":"Medium Italic",
                "bolditalic":"Bold Italic"
              },
          "Bold Italic":{  
                "roman":"Bold Italic",
                "bold":"Medium Italic",
                "italic":"Bold",
                "bolditalic":"Medium"
              },
          "Bold":{  
                "roman":"Bold",
                "bold":"Medium Italic",
                "italic":"Bold Italic",
                "bolditalic":"Medium"
              }
        },
    "Bell MT":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"null"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"null",
                  "bolditalic":"Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"null",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  }
        },
    "Californian FB":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"null"
                  },
			"Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"null",
                  "bolditalic":"Italic"
                  },
			"Italic":{  
                  "roman":"Italic",
                  "bold":"null",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  }
        },
    "DejaVu Sans":{
            "Condensed":{  
                  "roman":"Condensed",
                  "bold":"Condensed Bold",
                  "italic":"Condensed Oblique",
                  "bolditalic":"Condensed Bold Oblique"
                  },
              "Condensed Bold":{  
                  "roman":"Condensed Bold",
                  "bold":"Condensed",
                  "italic":"Condensed Bold Oblique",
                  "bolditalic":"Condensed Oblique"
                  },
              "Condensed Oblique":{  
                  "roman":"Condensed Oblique",
                  "bold":"Condensed Bold Oblique",
                  "italic":"Condensed",
                  "bolditalic":"Condensed Bold"
                  },
               "Condensed Bold Oblique":{  
                  "roman":"Condensed Bold Oblique",
                  "bold":"Condensed Oblique",
                  "italic":"Condensed Bold",
                  "bolditalic":"Condensed"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Book",
                  "italic":"Bold Oblique",
                  "bolditalic":"Oblique"
                  },
            "Oblique":{  
                  "roman":"Oblique",
                  "bold":"Bold Oblique",
                  "italic":"Book",
                  "bolditalic":"Bold"
                  },
            "Book":{  
                  "roman":"Book",
                  "bold":"Bold",
                  "italic":"Oblique",
                  "bolditalic":"Bold Oblique"
                  },
			"Bold Italic":{  
                  "roman":"Bold Oblique",
                  "bold":"Oblique",
                  "italic":"Bold",
                  "bolditalic":"Book"
                  },
              "ExtraLight":{  
                  "roman":"ExtraLight",
                  "bold":"Bold",
                  "italic":"Oblique",
                  "bolditalic":"Bold Oblique"
                  }
        },
    "DejaVu Serif":{
            "Condensed":{  
              "roman":"Condensed",
              "bold":"Condensed Bold",
              "italic":"Condensed Italic",
              "bolditalic":"Condensed BoldItalic"
              },
          "Condensed Bold":{  
                  "roman":"Condensed Bold",
                  "bold":"Condensed",
                  "italic":"Condensed BoldItalic",
                  "bolditalic":"Condensed Italic"
                  },
              "Condensed Italic":{  
                  "roman":"Condensed Italic",
                  "bold":"Condensed BoldItalic",
                  "italic":"Condensed",
                  "bolditalic":"Condensed Bold"
                  },
              "Bold Italic":{  
                  "roman":"Condensed BoldItalic",
                  "bold":"Condensed Italic",
                  "italic":"Condensed Bold",
                  "bolditalic":"Condensed"
                  },
          "Book":{  
              "roman":"Book",
              "bold":"Bold",
              "italic":"Italic",
              "bolditalic":"Bold Italic"
              },
          "Bold":{  
                  "roman":"Bold",
                  "bold":"Book",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Book",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Book"
                  }
          },
       "FSAlbert":{
            "Thin":{  
              "roman":"Thin",
              "bold":"Light",
              "italic":"ThinItalic",
              "bolditalic":"Light Italic"
              },
           "ThinItalic":{  
              "roman":"ThinItalic",
              "bold":"Light Italic",
              "italic":"Thin",
              "bolditalic":"Light"
              },
          "Light Italic":{  
              "roman":"Light Italic",
              "bold":"ThinItalic",
              "italic":"Light",
              "bolditalic":"Thin"
              },
          "Light":{  
              "roman":"Light",
              "bold":"Thin",
              "italic":"Light",
              "bolditalic":"ThinItalic"
              },
          "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
          },
      "Nueva Std":{
            "Condensed":{  
                  "roman":"Condensed",
                  "bold":"Bold Condensed",
                  "italic":"Condensed Italic",
                  "bolditalic":"Bold Condensed Italic"
                  },
              "Bold Condensed":{  
                  "roman":"Bold Condensed",
                  "bold":"Condensed",
                  "italic":"Bold Condensed Italic",
                  "bolditalic":"Condensed Italic"
                  },
              "Condensed Italic":{  
                  "roman":"Condensed Italic",
                  "bold":"Bold Condensed Italic",
                  "italic":"Condensed",
                  "bolditalic":"Bold Condensed"
                  },
               "Bold Condensed Italic":{  
                  "roman":"Bold Condensed Italic",
                  "bold":"Condensed Italic",
                  "italic":"Bold Condensed",
                  "bolditalic":"Condensed"
                  },
               "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
              "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }
              },
           "Segoe UI":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Semilight",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
              "Semilight":{  
                  "roman":"Semilight",
                  "bold":"Light",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  }
        },
     "Tekton Pro":{
            "Condensed":{  
                  "roman":"Condensed",
                  "bold":"Bold Condensed",
                  "italic":"Condensed Oblique",
                  "bolditalic":"Bold Condensed Oblique"
                  },
            "Bold Condensed":{  
                  "roman":"Bold Condensed",
                  "bold":"Condensed",
                  "italic":"Bold Condensed Oblique",
                  "bolditalic":"Condensed Oblique"
                  },
            "Condensed Oblique":{  
                  "roman":"Condensed Oblique",
                  "bold":"Bold Condensed Oblique",
                  "italic":"Condensed",
                  "bolditalic":"Bold Condensed"
                  },
            "Bold Condensed Oblique":{  
                  "roman":"Bold Condensed Oblique",
                  "bold":"Condensed Oblique",
                  "italic":"Bold Condensed",
                  "bolditalic":"Condensed"
                  },
              "Extended":{  
                  "roman":"Extended",
                  "bold":"Bold Extended",
                  "italic":"Extended Oblique",
                  "bolditalic":"Bold Extended Oblique "
                  },
            "Bold Extended":{  
                  "roman":"Bold Extended",
                  "bold":"Extended",
                  "italic":"Bold Extended Oblique ",
                  "bolditalic":"Extended Oblique"
                  },
            "Extended Oblique":{  
                  "roman":"Extended Oblique",
                  "bold":"Bold Extended Oblique ",
                  "italic":"Extended",
                  "bolditalic":"Bold Extended"
                  },
            "Bold Extended Oblique ":{  
                  "roman":"Bold Extended Oblique ",
                  "bold":"Extended Oblique",
                  "italic":"Bold Extended",
                  "bolditalic":"Extended"
                  }
              },
          "Garmond":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }				  
        },
          "Bodoni MT":{
            "Condensed":{  
                  "roman":"Condensed",
                  "bold":"Condensed Bold",
                  "italic":"Condensed Italic",
                  "bolditalic":"Condensed Bold Italic"
                  },
              "Condensed Bold":{  
                  "roman":"Condensed Bold",
                  "bold":"Condensed",
                  "italic":"Condensed Bold Italic",
                  "bolditalic":"Condensed Italic"
                  },
              "Condensed Italic":{  
                  "roman":"Condensed Italic",
                  "bold":"Condensed Bold Italic",
                  "italic":"Condensed",
                  "bolditalic":"Condensed Bold"
                  },
               "Condensed Bold Italic":{  
                  "roman":"Condensed Bold Italic",
                  "bold":"Condensed Italic",
                  "italic":"Condensed Bold",
                  "bolditalic":"Condensed"
                  },
              "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Black",
                  "italic":"Bold Italic",
                  "bolditalic":"Black Italic"
                  },
            "Black":{  
                  "roman":"Black",
                  "bold":"Bold",
                  "italic":"Black Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Black Italic",
                  "italic":"Black",
                  "bolditalic":"Bold"
                  },
            "Black Italic":{  
                  "roman":"Black Italic",
                  "bold":"Bold Italic",
                  "italic":"Black",
                  "bolditalic":"Bold"
                  },
                "Poster Compressed":{  
                  "roman":"Poster Compressed",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  }
              },
          "Horley Old Style MT Std":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }				  
        },
		"Noto Sans":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Bold",
                  "italic":"Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Regular",
                  "italic":"Bold Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Bold Italic",
                  "italic":"Regular",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Italic",
                  "italic":"Bold",
                  "bolditalic":"Regular"
                  }				  
        },
          "ITC Stone Sans Std":{
            "Medium":{  
                  "roman":"Medium",
                  "bold":"Semibold",
                  "italic":"Medium Italic",
                  "bolditalic":"Semibold Italic"
                  },
            "Semibold":{  
                  "roman":"Semibold",
                  "bold":"Medium",
                  "italic":"Semibold Italic",
                  "bolditalic":"Medium Italic"
                  },
            "Medium Italic":{  
                  "roman":"Medium Italic",
                  "bold":"Semibold Italic",
                  "italic":"Medium",
                  "bolditalic":"Semibold"
                  },
            "Semibold Italic":{  
                  "roman":"Semibold Italic",
                  "bold":"Medium Medium",
                  "italic":"Semibold",
                  "bolditalic":"Medium"
                  },
              "Bold":{  
                  "roman":"Bold",
                  "bold":"Medium",
                  "italic":"Bold Italic",
                  "bolditalic":"Medium Italic"
                  },
              "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Medium Italic",
                  "italic":"Medium",
                  "bolditalic":"Bold"
                  }
        },
    "Ortodoxa":{
            "Regular 3":{  
                  "roman":"Regular 3",
                  "bold":"Regular 8",
                  "italic":"Regular",
                  "bolditalic":"Regular 7"
                  },
              "Regular 4":{  
                  "roman":"Regular 4",
                  "bold":"Regular 8",
                  "italic":"Regular",
                  "bolditalic":"Regular 7"
                  },
              "Regular 6":{  
                  "roman":"Regular 6",
                  "bold":"Regular 8",
                  "italic":"Regular",
                  "bolditalic":"Regular 7"
                  },
            "Regular 8":{  
                  "roman":"Regular 8",
                  "bold":"Regular 3",
                  "italic":"Regular 7",
                  "bolditalic":"Regular"
                  },
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Regular 7",
                  "italic":"Regular 3",
                  "bolditalic":"Regular 8"
                  },
            "Regular 7":{  
                  "roman":"Regular 7",
                  "bold":"Regular",
                  "italic":"Regular 8",
                  "bolditalic":"Regular 3"
                  },
              "Regular 2":{  
                  "roman":"Regular 2",
                  "bold":"Regular",
                  "italic":"Regular 8",
                  "bolditalic":"Regular 3"
                  }
        },
    "Lato":{
            "Regular":{  
                  "roman":"Regular",
                  "bold":"Heavy",
                  "italic":"Italic",
                  "bolditalic":"Heavy Italic"
                  },
            "Heavy":{  
                  "roman":"Heavy",
                  "bold":"Regular",
                  "italic":"Heavy Italic",
                  "bolditalic":"Italic"
                  },
            "Italic":{  
                  "roman":"Italic",
                  "bold":"Heavy Italic",
                  "italic":"Regular",
                  "bolditalic":"Heavy"
                  },
            "Heavy Italic":{  
                  "roman":"Heavy Italic",
                  "bold":"Italic",
                  "italic":"Heavy",
                  "bolditalic":"Regular"
                  },
              "Light":{  
                  "roman":"Light",
                  "bold":"Bold",
                  "italic":"Light Italic",
                  "bolditalic":"Bold Italic"
                  },
            "Bold":{  
                  "roman":"Bold",
                  "bold":"Light",
                  "italic":"Bold Italic",
                  "bolditalic":"Light Italic"
                  },
            "Light Italic":{  
                  "roman":"Light Italic",
                  "bold":"Bold Italic",
                  "italic":"Light",
                  "bolditalic":"Bold"
                  },
            "Bold Italic":{  
                  "roman":"Bold Italic",
                  "bold":"Light Italic",
                  "italic":"Bold",
                  "bolditalic":"Light"
                  },
              "Thin":{  
                  "roman":"Thin",
                  "bold":"Semibold",
                  "italic":"Thin Italic",
                  "bolditalic":"Semibold Italic"
                  },
            "Semibold":{  
                  "roman":"Semibold",
                  "bold":"Thin",
                  "italic":"Semibold Italic",
                  "bolditalic":"Thin Italic"
                  },
            "Italic":{  
                  "roman":"Thin Italic",
                  "bold":"Semibold Italic",
                  "italic":"Thin",
                  "bolditalic":"Semibold"
                  },
            "Semibold Italic":{  
                  "roman":"Semibold Italic",
                  "bold":"Thin Italic",
                  "italic":"Semibold",
                  "bolditalic":"Thin"
                  },
              "Medium":{  
                  "roman":"Medium",
                  "bold":"Black",
                  "italic":"Medium Italic",
                  "bolditalic":"Black Italic"
                  },
            "Black":{  
                  "roman":"Black",
                  "bold":"Medium",
                  "italic":"Black Italic",
                  "bolditalic":"Medium Italic"
                  },
            "Medium Italic":{  
                  "roman":"Medium Italic",
                  "bold":"Black Italic",
                  "italic":"Medium",
                  "bolditalic":"Black"
                  },
            "Black Italic":{  
                  "roman":"Black Italic",
                  "bold":"Medium Italic",
                  "italic":"Black",
                  "bolditalic":"Medium"
                  },
              "Hairline":{  
                  "roman":"Hairline",
                  "bold":"Medium",
                  "italic":"Hairline Italic",
                  "bolditalic":"Medium Italic"
                  },
            "Hairline Italic":{  
                  "roman":"Hairline Italic",
                  "bold":"Medium Italic",
                  "italic":"Hairline",
                  "bolditalic":"Medium"
                  }
        }
}
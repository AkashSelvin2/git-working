﻿//Developer: Bharanitharan P
//Functionallity: Validating template with required config file
var myDoc = app.activeDocument;
var doc_name=myDoc.name;
var filePath = myDoc.filePath;// get indesign template path
var scriptpath = filePath.toString().replace("Templates/_cache/templates","SupportScripts/_cache/scripts");//get config script path
//============================
var os = $.os.toString().split(' ');
var currentOs =  os[0].toString();
//============================
//create log folder 
if(currentOs == 'Windows'){
var f = new Folder(filePath +"\\TEMPLATE_VALIDATION_REPORT\\"); // create a new directory to store log details in windows
}
else{
var f = new Folder(filePath +"//TEMPLATE_VALIDATION_REPORT//"); // create a new directory to store log details in mac
}
f.create();
//create a error log file
if(currentOs == 'Windows'){
var logTxt = new File (filePath +"\\TEMPLATE_VALIDATION_REPORT\\" + doc_name.replace(".indt",".log"));
}
else{
var logTxt = new File (filePath +"//TEMPLATE_VALIDATION_REPORT//" + doc_name.replace(".indt",".log"));
}
logTxt.open('w');
//append the config file
var commonScriptName = $.fileName;//get current script file
commonScriptName = commonScriptName.replace(/.*\/(.+.jsx)/, "$1");// get the current script file name
commonScriptName = commonScriptName.replace('.jsx', '_config.jsx');
layerTemplateScript = File($.fileName).parent.fsName//File(getActiveScriptPath()).parent.fsName;
var configFileName =  myDoc.name.replace("indesignAutoPage.indt","indesignAutoPageConfig.jsx");// get template name as a config name
// get selected customer config
if(currentOs == 'Windows'){
var scriptFile = (File(scriptpath + "\\" +configFileName));
}
else{
var scriptFile = (File(scriptpath + "//" +configFileName));
}
var script = '#include' + scriptFile.fullName;
eval(script);   
//get the config commonly
if(currentOs == 'Windows'){
var commonScriptFile = (File(layerTemplateScript + "\\"+commonScriptName));
}
else{
var commonScriptFile = (File(layerTemplateScript + "//"+commonScriptName));
}
var commonScript = '#include' + commonScriptFile.fullName;
eval(commonScript);   
var noError =true;
//---------------------------------------------------------------------------------------------------------------------------------------------------------|
// Get local time, date and year.
var digital = new Date();
var date = digital.getMonth()+1;
if(date < 10){
    date = "0"+date;
}
var todaysDate = digital.toLocaleTimeString()+", "+digital.getDate()+"-"+date+"-"+parseInt(digital.getYear()+1900);
//---------------------------------------------------------------------------------------------------------------------------------------------------------| 
if(currentOs == 'Windows'){
var currentUser = $.getenv("USERNAME");
}
else{
var currentUser = $.getenv("USER");
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------|
var ruleOrg= myDoc.viewPreferences.rulerOrigin;
 myDoc.viewPreferences.rulerOrigin = RulerOrigin.pageOrigin; 
//---------------------------------------------------------------------------------------------------------------------------------------------------------|
//Style Validation for paragraph | character | object | textFrame | masterSpreadSheet
var styleIndex = commonConfig.styleValidation.styleType;
for(styletype=0; styletype < styleIndex.length;styletype++){//looping styleType
    var styleNamel=styleIndex[styletype];
    styleValidation(styleNamel);//parse the name of object from the config file
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------|
//checking wether document has 3 pages
var pages = myDoc.pages;
if (myDoc.pages.length != 3){ // checking wether document has 3 pages
    logTxt.write("\n\n"+"Document pages not equal to 3. " + "\n");
    noError = false;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------|
//style validation for existing style related to paragraph | character | object styles 
validationRelatedToBaseStyle("paragraphStyles","BL-T", "BL-T-AFTER-HEAD")
validationRelatedToBaseStyle("paragraphStyles","DL-T", "DL-T-AFTER-HEAD")
validationRelatedToBaseStyle("paragraphStyles","NL-T", "NL-T-AFTER-HEAD")
validationRelatedToBaseStyle("paragraphStyles","AL-T", "AL-T-AFTER-HEAD")
validationRelatedToBaseStyle("paragraphStyles","APP_EQN-T", "APP_EQN-O-AFTER-HEAD|APP_EQN-O-BEFORE-HEAD")
validationRelatedToBaseStyle("paragraphStyles","APP_jrnlTblHead", "APP_jrnlTblHead_CTR|APP_jrnlTblHead_RGT")
validationRelatedToBaseStyle("paragraphStyles","BOX_QUOTE-T", "BOX_QUOTE-O")
validationRelatedToBaseStyle("paragraphStyles","QUOTE-T", "QUOTE-O")
validationRelatedToBaseStyle("paragraphStyles","EQN", "TXT_AFTER_EQN")
validationRelatedToBaseStyle("paragraphStyles","jrnlAppHead2", "jrnlAppHead2_After_OL|jrnlAppHead2_After_UL")
validationRelatedToBaseStyle("paragraphStyles","TBL_NL-T", "TBL_NL-O")
validationRelatedToBaseStyle("paragraphStyles","TBL_BL-T", "TBL_BL-O")
validationRelatedToBaseStyle("paragraphStyles","QUOTE-T", "QUOTE-O-AFTER-QUOTE")
validationRelatedToBaseStyle("paragraphStyles","NL-T", "NL-T-AFTER-HEAD2")
validationRelatedToBaseStyle("cellStyles","TB_FILL","TB_ROT_VER_CTR_FILL|TB_ROT_VER_BTM_FILL|TB_ROT_VER_FILL")
validationRelatedToBaseStyle("cellStyles","TCH_FILL","TCH_ROT_VER_CTR_FILL|TCH_ROT_VER_BTM_FILL|TCH_ROT_VER_FILL")
validationRelatedToBaseStyle("cellStyles","TBL_FILL","TBL_ROT_VER_CTR_FILL|TBL_ROT_VER_BTM_FILL|TBL_ROT_VER_FILL")
validationRelatedToBaseStyle("cellStyles","TBF_FILL","TBF_ROT_VER_CTR_FILL|TBF_ROT_VER_BTM_FILL|TBF_ROT_VER_FILL")
validationRelatedToBaseStyle("cellStyles","TB_FILL","TB_ROT_DIA_CTR_FILL|TB_ROT_DIA_BTM_FILL|TB_ROT_DIA_FILL")
validationRelatedToBaseStyle("cellStyles","TCH_FILL","TCH_ROT_DIA_CTR_FILL|TCH_ROT_DIA_BTM_FILL|TCH_ROT_DIA_FILL")
validationRelatedToBaseStyle("cellStyles","TBL_FILL","TBL_ROT_DIA_CTR_FILL|TBL_ROT_DIA_BTM_FILL|TBL_ROT_DIA_FILL")
validationRelatedToBaseStyle("cellStyles","TBF_FILL","TBF_ROT_DIA_CTR_FILL|TBF_ROT_DIA_BTM_FILL|TBF_ROT_DIA_FILL")
validationRelatedToBaseStyle("characterStyles","NO_SUP_jrnlBibRef","NO_SUP_jrnlTblBodyBibRef|NO_SUP_jrnlTblFootBibRef|NO_SUP_jrnlBibRefFigFoot")
validationRelatedToBaseStyle("characterStyles","EXP-italic","jrnlMathBaselineShift_0.1|jrnlMathBaselineShift_99.9")
//---------------------------------------------------------------------------------------------------------------------------------------------------------|
//layers - validate textFrame for every layer of the name starts with 'LAYOUT'
var pattern = new RegExp("^LAYOUT");
var layerDefault = new Array("LAYOUT1","COMMON");
var layer = myDoc.layers;
for(layerDef=0; layerDef < layerDefault.length ; layerDef++){// check whether the default layers of 'LAYER' and 'COMMON' are present or not
    if(!layer.itemByName(layerDefault[layerDef]).isValid){
        logTxt.write("\n\n" +"Missing Default Layer - "+ layerDefault[layerDef] + "\n");
        noError = false;
    }
}
for(offLayer=0; offLayer < layer.length; offLayer++){//iinitially set  all layers of 'LAYOUT' visible = false
var layerObjOff = layer[offLayer];
    if(pattern.test(layerObjOff.name)){ 
        layerObjOff.visible = false;
    }
}
for(lay=0; lay < layer.length; lay++){// change active 'LAYOUT' visible = true
var layerObj = layer[lay];
var layerName = layerObj.name; 
    if(pattern.test(layerName)){  
        layerObj.visible = true;
        masterSpread(layerName);// parse active layer of 'LAYOUT' in the params
        layerObj.visible = false;
    }
}
for(onLayer=0; onLayer < layer.length; onLayer++){//change all layers of 'LAYOUT' visible = true
var layerObjOn = layer[onLayer];
    if(pattern.test(layerObjOn.name)){ 
        layerObjOn.visible = true;
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------|
//check paragraph style in config 
var paraIncAft = config.adjustParagraphSpaceStyleList.increase.after.split(",");// string to array
var paraIncBef = config.adjustParagraphSpaceStyleList.increase.before.split(",");
var paraDecAft = config.adjustParagraphSpaceStyleList.decrease.after.split(",");
var paraDecBef = config.adjustParagraphSpaceStyleList.decrease.before.split(",");
var paraInc = paraIncBef.concat(paraIncAft);
var paraDec = paraDecBef.concat(paraDecAft);
var paraStyle = paraInc.concat(paraDec).sort();
//validate adjustParagraphSpaceStyleList in config
var paraObjArray={};
var title = true;
for(par=0; par < paraStyle.length; par++){
    if(typeof(paraObjArray[paraStyle[par]]) === 'undefined'){
        if(!myDoc.paragraphStyles.itemByName(paraStyle[par]).isValid && paraObjArray[paraStyle[par].toString()] != undefined){
        paraObjArray[paraStyle[par].toString()] = false;
            if(title){
            logTxt.write("\n\n"+ "Missing adjustParagraphSpaceStyleList :");
            title =false; //to avoid error title repeating
            noError = false;
            } 
          logTxt.write("\n" + paraStyle[par]);
        }else{
        paraObjArray[paraStyle[par].toString()] = true;
        }
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------|
//validate detailsForVJ in config
var detailsForVJ = config.detailsForVJ.stylesExcludedForVJ.split(",").sort();// sort the array
var detailsObjArray={};
var title = true;
for(detVJ=0; detVJ < detailsForVJ.length; detVJ++){
    if(typeof(detailsObjArray[detailsForVJ[detVJ]]) === 'undefined'){
        if(!myDoc.paragraphStyles.itemByName(detailsForVJ[detVJ]).isValid){
        detailsObjArray[detailsForVJ[detVJ].toString()] = false;
            if(title){
                logTxt.write("\n\n" +"Missing detailsForVJ :");
                title =false;
                noError = false;
            }
          logTxt.write("\n" + detailsForVJ[detVJ]);
        }else{
        detailsObjArray[detailsForVJ[detVJ].toString()] = true;
        }
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------| 
//Description :  Validating mandatory text frames are there for each layout
//Created On : (09-01-2019)
var layer = myDoc.layers;
var allLayer = myDoc.layers;
var layerLen = allLayer.length;
var layoutArray =[];
var pattern = new RegExp("^LAYOUT");
var configLayout = config.pageColumnDetails;
// collect the layout names which is matching with pattern of 'LAYOUT' and stored in an array. 
for(var lay=0; lay <layer.length; lay++){
    var layoutOnly = layer[lay];
    if(pattern.test(layoutOnly.name)){
        layoutArray.push(layoutOnly.name);
    }
}
//  validating the layout one bye one
for(var allLay=0; allLay < app.activeDocument.layers.length; allLay++){
    var layoutName = app.activeDocument.layers[allLay].name; // keep current layout
     if(pattern.test(layoutName)){
         for(var layArr=0; layArr < layoutArray.length; layArr++){
             if(layoutName != layoutArray[layArr]){
                 app.activeDocument.layers.itemByName(layoutArray[layArr]).remove(); // remove all layout except the current layout
             }
         }
    masterSpreadTextframeCheck(layoutName);// call the function with current LAYOUT
    app.scriptPreferences.userInteractionLevel = UserInteractionLevels.neverInteract;  
    app.activeDocument.revert(); // revert all the process
     }
} 
//---------------------------------------------------------------------------------------------------------------------------------------------------------|
//Description :  Validate objectStyle autosizing
//Created On : (12-01-2019)
var objectStyleArray = new Array("TBL","TBL_Cont","TBL_CONT_TOP","TBL_CONT_BOTTOM","FIG");
var objStylTitle =true;
for(var objArr=0; objArr < objectStyleArray.length; objArr++){
    var objStylName= objectStyleArray[objArr];
    if(app.activeDocument.objectStyles.itemByName(objStylName).isValid){
        var objStyleTextFrame =app.activeDocument.objectStyles.itemByName(objStylName).textFramePreferences;
        var objStylAutoSizeType = objStyleTextFrame.autoSizingType;
        var objStylAutoSizeRefPoint = objStyleTextFrame.autoSizingReferencePoint;
        if(objStylAutoSizeType.toString() === "OFF"){
            if(objStylAutoSizeRefPoint.toString() != "TOP_CENTER_POINT"){
            if(objStylTitle){
                 logTxt.write("\n\nObjectstyle Textframe Autosizing");
                 objStylTitle = false;
                }
            logTxt.write("\n" + objStylName+ " autosizing refernce point is not in 'TOP_CENTER_POINT'");
            }
        }
        else{
             if(objStylTitle){
                 logTxt.write("\n\nObjectstyle Textframe Autosizing");
                 objStylTitle = false;
            }
            logTxt.write("\n"+objStylName+" autosizing type is not in default 'OFF' mode");
        }
    }
}
if(!objStylTitle){//Global error log check
    noError = false;
}
//=========================================================================================|
//Description :  Validate for duplicate first frame in template, updated by aNTON, dt: 06-Oct-2020
//=========================================================================================|
var myDoc = app.activeDocument;
var docLayers = myDoc.layers;
var docLayersLen = docLayers.length;
myDoc.activeLayer = "COMMON";
var frameRepeat =true;
for (var lr = docLayersLen - 1; lr >=0; lr --){
    var frameCount = 0;
    var labelCount = 0;
    var currLayer = docLayers[lr];
    var currLayerName = currLayer.name;
    currLayer.locked = false;
    currLayer.visible = true;
    var textFrame = currLayer.textFrames;
    var textFrameLen = textFrame.length;
    for(var tf = 0; textFrameLen > tf; tf++){
        var currTextFrame = textFrame[tf];
        if(currTextFrame.name == "FIRST_FRAME"){
            frameCount = frameCount + 1;
            }
        if(currTextFrame.label == "FIRST_FRAME"){
            labelCount = labelCount + 1;
            }
        }
    if(frameCount > 1){
        if(frameRepeat){
             logTxt.write("\n\nDuplicate FIRST_FRAME in layers: ");
             frameRepeat = false;
            }
       logTxt.write("\n"+currLayerName+" - remove duplicate frame name from this layer");
        }
    if(labelCount > 1){
        if(frameRepeat){
             logTxt.write("\n\nDuplicate FIRST_FRAME in layers: ");
             frameRepeat = false;
            }
       logTxt.write("\n"+currLayerName+" - remove duplicate label from this layer");
        }
    }
if(!frameRepeat){//Global error log check
    noError = false;
    }
//=========================================================================================|
//Description :  Validate the character style font variant and family
                    //1) Collecting the character styles based on paragraph styles
                    //2) Evaluate the paragraph style font family and font variant with fontVariantConfig.jsx 
                    //3) Evaluate the character style font family and font variant with based on paragraph styles
                    //4) Log the error i) Missing fonts in config ii) Template fonts conflict with config iii) Missing font family and font variant in character styles iv) Missing font family and font variant in user system
//Developed by : aNTON 
//Created On : 21-Nov-2019
//=========================================================================================|
var currDoc = app.activeDocument;
var fontConfigName = $.fileName;//get current script file
fontConfigName = fontConfigName.replace('templateValidator.jsx', 'FontVariantConfig.jsx');
var commonFontScriptFile = (File(fontConfigName));
var configPathName = '#include' + commonFontScriptFile.fullName;
eval(configPathName);  
var styleIssue = true;
var variantsArray = new Array(/(^\w+)(BoldItalic$)/,/(^\w+)(BoldItalicSup$)/,/(^\w+)(BoldItalicSub$)/,/(^\w+)(Bold$)/,/(^\w+)(BoldSup$)/,/(^\w+)(BoldSub$)/,/(^\w+)(Italic$)/,/(^\w+)(ItalicSup$)/,/(^\w+)(ItalicSub$)/,/(^\EXP-)(.*)/);// regex for character styles name matching end with Bold Italic, Italic and bold; start with EXP-
var cstyles = currDoc.characterStyles;
var cstylesLen = cstyles.length;
for(var varArr=0; varArr < variantsArray.length; varArr++){
    var fontVariant= variantsArray[varArr];
    for (var cs = 1; cs < cstylesLen; cs++){// collecting the CS style
        var currCStyle = cstyles[cs];
        var currCStyleName = currCStyle.name;// CS style name
        var regPattern = new RegExp(fontVariant);
        var matchPstyle = regPattern.test(currCStyleName);// matching the current cs style name and font variant
        if(matchPstyle){//if matchStyle is true and then first group added in one variable and second group added in another variable
            var currReqPStyle = currCStyleName.replace(fontVariant,'$1');
            var currVariant = currCStyleName.replace(fontVariant,'$2');
            var matchStyle1 = currVariant.toString().match("Sup");//handling superscript cstyles
            var matchStyle2 = currVariant.toString().match("Sub");//handling subscript cstyles
            if(matchStyle1 == "Sup" || matchStyle2 == "Sub"){
                currVariant = currVariant.replace(/(^\w+)(Sub$|Sup$)/,'$1');
                currVariant = currVariant.toLowerCase();
                }
            else{
                currVariant = currVariant.toLowerCase();
                }
            if (currDoc.paragraphStyles.itemByName(currReqPStyle).isValid && (currVariant == "bolditalic" || currVariant == "bold" || currVariant == "italic")){// here we have processing the PS styles based CS styles
                var currPStyle = currDoc.paragraphStyles.itemByName(currReqPStyle);
                characterStyleValidation(currCStyle,currPStyle,currVariant);//call the validation function and pass the three parameters i) current PS style ii) current CS style iii) current variant
                }
            else if (currReqPStyle == 'EXP-'){// processing the EXP CS styles
                var currPStyle = currDoc.paragraphStyles.itemByName("TXT");
                var currVariantList = currVariant.split('-');
                var currVariantLen = currVariantList.length;
                if (currVariantList[0] == 'bold' && currVariantList[1] == 'italic'){
                    currVariant = currVariantList[0]+currVariantList[1];
                    characterStyleValidation(currCStyle,currPStyle,currVariant);//call the validation function and pass the three parameters i) current PS style ii) current CS style iii) current variant
                    }
                else if (currVariantList[0] == 'bold' || currVariantList[0] == 'italic'){
                    currVariant = currVariantList[0];
                    characterStyleValidation(currCStyle,currPStyle,currVariant);//call the validation function and pass the three parameters i) current PS style ii) current CS style iii) current variant
                    }
                }
            }// end else if
        }//end for loop
    }//end for loop
function characterStyleValidation(currCStyle,currPStyle,currVariant){
    var currDoc = app.activeDocument;
    try{
         var currPStyleFontFamily=currPStyle.appliedFont.fontFamily;
         var matchPSFontFamily = regPattern.test(currPStyleFontFamily)
         currPStyleFontFamily = currPStyleFontFamily.replace(/(^.*)(\s\(.*\)$)/,'$1');// removing the (TT), (OTF), (T1) at end of font family for validation purpose
         //validate the runOn head character styles based on config, update by aNTON,dt 15-Sep-2020
          if(typeof(config.validatingRunOnHead) != 'undefined' && typeof(config.validatingRunOnHead[currPStyle.name]) !='undefined' && config.validatingRunOnHead[currPStyle.name] == true){
            if(currDoc.characterStyles.itemByName(currPStyle.name).isValid){
                try{
                    var currCStyleFontFy=currDoc.characterStyles.itemByName(currPStyle.name).appliedFont;
                    if(currCStyleFontFy != ''){
                        currCStyleFontFy = currCStyleFontFy.replace(/(^.*)(\s\(.*\)$)/,'$1');
                        currPStyleFontFamily = currCStyleFontFy;
                        }
                    }catch(e){}
                }
            }
         }
     catch(e){
         if (styleIssue){
             logTxt.write("\n\nFont issues in CHARACTER STYLES : ");
             styleIssue = false;
             }
         logTxt.write("\n"+currCStyle.name+": font family defined in \'"+currPStyle.name+"\' paragraph style is not available in this system");
         noError = false;
        }
    try{
         var currPStyleFontStyle=currPStyle.appliedFont.fontStyleName;
         //validate the runOn head character styles based on config, update by aNTON,dt 15-Sep-2020
         if(typeof(config.validatingRunOnHead) != 'undefined' && typeof(config.validatingRunOnHead[currPStyle.name]) !='undefined' && config.validatingRunOnHead[currPStyle.name] == true){
            if(currDoc.characterStyles.itemByName(currPStyle.name).isValid){
                try{
                    var currCStyleFontVariant=currDoc.characterStyles.itemByName(currPStyle.name).fontStyle;
                    currPStyleFontStyle = currCStyleFontVariant;
                    }catch(e){}
                }
            }
        }
     catch(e){
         if (styleIssue){
             logTxt.write("\n\nFont issues in CHARACTER STYLES : ");
             styleIssue = false;
             }
         logTxt.write("\n"+currCStyle.name+": font variant defined in \'"+currPStyle.name+"\' paragraph style is not available in this system");
         noError = false;
        }
    try{
         var currCStyleFontFamily=currCStyle.appliedFont;
         currCStyleFontFamily = currCStyleFontFamily.replace(/(^.*)(\s\(.*\)$)/,'$1');
         }
     catch(e){
         if (styleIssue){
             logTxt.write("\n\nFont issues in CHARACTER STYLES : ");
             styleIssue = false;
             }
         logTxt.write("\n"+currCStyle.name+": font family defined in character style is not available in this system");
         noError = false;
        }
    try{
         var currCStyleFontStyle=currCStyle.fontStyle;
        }
     catch(e){
         if (styleIssue){
             logTxt.write("\n\nFont issues in CHARACTER STYLES : ");
             styleIssue = false;
             }
         logTxt.write("\n"+currCStyle.name+": font variant defined in character style is not available in this system");
         noError = false;
        }
    if (typeof(commonConfig[currPStyleFontFamily]) == 'undefined'){
        if (styleIssue){
             logTxt.write("\n\nFont issues in CHARACTER STYLES : ");
             styleIssue = false;
             }
        logTxt.write("\n" +currCStyle.name+ ": \'"+currPStyleFontFamily+"\' font family is not defined in config");
        noError = false;
        }
    else if (typeof(commonConfig[currPStyleFontFamily][currPStyleFontStyle]) == 'undefined' ){
        if (styleIssue){
             logTxt.write("\n\nFont issues in CHARACTER STYLES : ");
             styleIssue = false;
             }
        logTxt.write("\n" +currCStyle.name+ ": font variant \'"+currPStyleFontStyle+"\' for the font \'"+currPStyleFontFamily+"\' is not defined in config");
        noError = false;
        }
     else if (commonConfig[currPStyleFontFamily][currPStyleFontStyle][currVariant]){
         if(commonConfig[currPStyleFontFamily][currPStyleFontStyle][currVariant].fontVariant != undefined && commonConfig[currPStyleFontFamily][currPStyleFontStyle][currVariant].fontFamily != undefined){
             if(commonConfig[currPStyleFontFamily][currPStyleFontStyle][currVariant].fontVariant != currCStyle.fontStyle && commonConfig[currPStyleFontFamily][currPStyleFontStyle][currVariant].fontFamily != currCStyleFontFamily){
                  if (styleIssue){
                     logTxt.write("\n\nFont issues in CHARACTER STYLES : ");
                     styleIssue = false;
                    }
                logTxt.write("\n" +currCStyle.name+ ": font variant conflicts with config");
                noError = false;
                 }
             }
         else if(commonConfig[currPStyleFontFamily][currPStyleFontStyle][currVariant] == currCStyle.fontStyle && currPStyleFontFamily != currCStyleFontFamily && currCStyleFontFamily != ''){
             if (styleIssue){
             logTxt.write("\n\nFont issues in CHARACTER STYLES : ");
             styleIssue = false;
                }
            logTxt.write("\n" +currCStyle.name+ ": has incorrect font family.");
            noError = false;
            }
        else if (commonConfig[currPStyleFontFamily][currPStyleFontStyle][currVariant] != currCStyle.fontStyle){
            if (styleIssue){
             logTxt.write("\n\nFont issues in CHARACTER STYLES : ");
             styleIssue = false;
                }
            logTxt.write("\n" +currCStyle.name+ ": font variant conflicts with config");
            noError = false;
            }
        }// end if
    }
//=========================================================================================|
//Description :  Validate the reference character styles based on styleTemplate.xml
                    //1) Collecting the reference character styles based on styleTemplate.xml
                    //2) Removing the duplicate style from collected styles 
                    //3) Spliting the missing reference styles and existing styles from template and log an error for missing styles
                    //4) Evaluate existing style and log an errors i) Missing fonts in config ii) Template fonts conflict with config iii) Missing font family and font variant in character styles iv) Missing font family and font variant in user system v) undefined ref style in the Template config
//Developed by : aNTON 
//Created On : 31-Dec-2019
//=========================================================================================|
var myDoc = app.activeDocument;
var doc_name = myDoc.name;
app.scriptPreferences.userInteractionLevel = UserInteractionLevels.NEVER_INTERACT;
var filePath = myDoc.filePath;
var styleIssue = true;
//Creating the VBS file for colllection the styleTemplate.xml from kriya site
var vbsFile = new File (filePath + "\\"+doc_name.replace(doc_name,'getStyleTemplate.vbs'));
var XMLfile = filePath+"\\styleTemplate.xml";
//Evaluate the master font file
var fontConfigName = $.fileName;//get current script file
fontConfigName = fontConfigName.replace('templateValidator.jsx', 'FontVariantConfig.jsx');
var commonFontScriptFile = (File(fontConfigName));
var configPathName = '#include' + commonFontScriptFile.fullName;
eval(configPathName);  
//Getting the downloading styleTemplate path 
var styleTemplatePath = filePath.toString().replace('/','\\','ig');
matchStylePath=styleTemplatePath.toString().replace(/(^\\)/,'');
styleTemplatePath = matchStylePath.toString().replace(/(^[a-zA-Z]+)(\\)/,'$1:\\');
//Getting the clinent name from template folder
var clientName = filePath.toString().split('/');
clientName = clientName[clientName.length-1];
var jrnlName;
var jrnlNameListLen = 0
// here we are collecting the jounal list from template
if(myDoc.textVariables.item("projectName").isValid){    
    var jrnlName = myDoc.textVariables.item("projectName").variableOptions.contents;
    var jrnlNameList = jrnlName.split(',')
    }
else if (myDoc.textVariables.item("TEMPLATE_ID").isValid){
    var jrnlName = myDoc.textVariables.item("TEMPLATE_ID").variableOptions.contents;
    var jrnlNameList = jrnlName.toLowerCase().split('_')
    }
var jrnlNameListLen = jrnlNameList.length;
var xmlNode = myDoc.xmlElements[0].evaluateXPathExpression("//template");
var xmlNodeLen = xmlNode.length;
if(jrnlNameListLen > 0){
    for (var pn =0; pn < jrnlNameListLen; pn++){
        var currJrnlName = jrnlNameList[pn];
        //if styleTemplate and VBS files are already in folder, we removed and recreated
        if (File(XMLfile).exists){
            File(XMLfile).remove();
            }
        if (xmlNodeLen > 0){
            for (t =0; t < xmlNodeLen; t++){
                xmlNode[t].untag();
                }
            }
        if (File(vbsFile).exists){
            File(vbsFile).remove();
            }
        //Writing the VBS file based on client and jornal name
        vbsFile.open('w');
        vbsFile.write("Dim oXMLHTTP \nDim oStream\n\nSet oXMLHTTP = CreateObject(\"MSXML2.XMLHTTP.3.0\")");
        vbsFile.write("\n\noXMLHTTP.Open \"GET\", \"https://kriya2.kriyadocs.com/api/getrefcomponent?apiKey=36ab61a9-47e1-4db6-96db-8b95a9923599&type=getStyleTemplate&customer=");
        vbsFile.write(clientName);
        vbsFile.write("&project=");
        vbsFile.write(currJrnlName);
        vbsFile.write("\", False");
        vbsFile.write("\noXMLHTTP.Send");
        vbsFile.write("\n\nIf oXMLHTTP.Status = 200 Then");
        vbsFile.write("\n\tSet oStream = CreateObject(\"ADODB.Stream\")");
        vbsFile.write("\n\toStream.Open");
        vbsFile.write("\n\toStream.Type = 1");
        vbsFile.write("\n\toStream.Write oXMLHTTP.responseBody");
        vbsFile.write("\n\toStream.SaveToFile ");
        vbsFile.write("\""+styleTemplatePath+"\\styleTemplate.xml\"");
        vbsFile.write("\n\toStream.Close");
        vbsFile.write("\nEnd If\n");
        vbsFile.close();
        app.doScript(File(filePath+'/getStyleTemplate.vbs'), ScriptLanguage.visualBasic);
        if(File(XMLfile).exists){
            myDoc.importXML(File(XMLfile));
            var refStylesArray = [];
            var refStylesFinalArray = [];
            var refExistStyleArray = [];
            //Collection reference character style from styleTemplate.xml
            var refTypeNode = myDoc.xmlElements[0].evaluateXPathExpression("//refType");
            for (var nod=0; nod < refTypeNode.length; nod++){
                var currRefTypeNode = refTypeNode[nod].xmlElements;   
                for (var tag=0; tag < currRefTypeNode.length; tag++){
                    refStylesArray.push(currRefTypeNode[tag].markupTag.name);
                    }
                }
            //Removing duplicate reference character style from collected array
            var count = 0;
            var start = false; 
            for (rsa = 0; rsa < refStylesArray.length; rsa++) { 
                for (rsfa = 0; rsfa < refStylesFinalArray.length;rsfa++) { 
                    if (refStylesArray[rsa] == refStylesFinalArray[rsfa]) { 
                        start = true; 
                        } 
                    } 
                    count++; 
                    if (count == 1 && start == false) { 
                        refStylesFinalArray.push(refStylesArray[rsa]); 
                        } 
                    start = false; 
                    count = 0; 
                    } 
                //Spliting the missing reference styles and existing styles from template
                var cstyles = myDoc.characterStyles;
                var cstylesLen = cstyles.length;
                for(var refArr=0; refArr < refStylesFinalArray.length; refArr++){
                    var currRefStyle= refStylesFinalArray[refArr];
                    for (var cs = 1; cs < cstylesLen; cs++){// collecting the CS styke
                        var currCStyle = cstyles[cs];
                        if(currCStyle.name == currRefStyle && myDoc.characterStyles.itemByName(currRefStyle).isValid){
                           refExistStyleArray.push(currCStyle);
                           break;
                           }
                        else if (!myDoc.characterStyles.itemByName(currRefStyle).isValid){
                            if (styleIssue){
                                 logTxt.write("\n\nMissing REFERENCE CHARACTER STYLES : "+currJrnlName);
                                 styleIssue = false;
                                 }
                            logTxt.write("\n"+currRefStyle);
                            noError = false;
                            break;
                            }
                        }//end of for loop
                    }//end of for loop
                characterRefStyleValidation(refExistStyleArray)
                }
        else{
            alert(currJrnlName+": StyleTemplate.xml file is not found in specifc path OR please add the new text variable 'projectName' and listdown jornal name with comma separator")
            }
        }//end of for loop
   
    }//end of if condition
//Once validation completed, styleTemplate.xml and getStyleTemplate.vbs files removed from template folder
if (File(XMLfile).exists){
    File(XMLfile).remove();
    }
if (xmlNodeLen > 0){
    for (t =0; t < xmlNodeLen; t++){
        xmlNode[t].untag();
        }
    }
if (File(vbsFile).exists){
    File(vbsFile).remove();
    }

function characterRefStyleValidation(refExistStyleArray){
    styleIssue = true;
    for(arr =0; arr < refExistStyleArray.length; arr++){
        var currCSArrayName = refExistStyleArray[arr].name;
        var myPStyle = myDoc.paragraphStyles.itemByName("jrnlRefText");
        var myCStyle = myDoc.characterStyles.itemByName(currCSArrayName);
        try{
            var myPStyleFontFamily=myPStyle.appliedFont.fontFamily;
            myPStyleFontFamily = myPStyleFontFamily.replace(/(^.*)(\s\(.*\)$)/,'$1');
            }
        catch(e){
            logTxt.write("\n"+myPStyle.name+": Font family defined, but is not available in this system");
            noError = false;
            }
        try{
            var myPStyleFontStyle=myPStyle.appliedFont.fontStyleName;
            }
        catch(e){
            logTxt.write("\n"+myPStyle.name+": Font variant defined, but is not available in this system");
            noError = false;
            }
        try{
             var myCStyleFontFamily=myCStyle.appliedFont;
             myCStyleFontFamily = myCStyleFontFamily.replace(/(^.*)(\s\(.*\)$)/,'$1');
             }
        catch(e){
            logTxt.write("\n"+myCStyle.name+": Font family defined in character style is not available in this system");
            noError = false;
            }
        try{
             var myCStyleFontStyle=myCStyle.fontStyle;
            }
         catch(e){
             logTxt.write("\n"+myCStyle.name+": font variant defined in character style is not available in this system");
             noError = false;
            }
         if (typeof(commonConfig[myPStyleFontFamily]) == 'undefined'){
             if (styleIssue){
                 logTxt.write("\n\nFont issues in REFERENCE CHARACTER STYLES : ");
                 styleIssue = false;
                 }
                logTxt.write("\n" +myCStyle.name+ ": \'"+myPStyleFontFamily+"\' font family is not defined in config");
                noError = false;
                }
          else if (typeof(commonConfig[myPStyleFontFamily][myPStyleFontStyle]) == 'undefined' ){
           if (styleIssue){
                 logTxt.write("\n\nFont issues in REFERENCE CHARACTER STYLES : ");
                 styleIssue = false;
                 }
                logTxt.write("\n" +myCStyle.name+ ": font variant \'"+myPStyleFontStyle+"\' for the font \'"+myPStyleFontFamily+"\' is not defined in config");
                noError = false;
            }
         else if (myCStyleFontFamily == myPStyleFontFamily){
            if (myCStyleFontStyle != NothingEnum.nothing){
                if(commonConfig[myPStyleFontFamily][myCStyleFontStyle] == undefined){
                     logTxt.write("\n" +myCStyle.name+ ": font variant \'"+myCStyleFontStyle+"\' is not defined in Config");
                     noError = false;
                    }
                }
            }
         else if (myCStyleFontFamily != myPStyleFontFamily && myCStyleFontStyle != NothingEnum.nothing ){
            if(myCStyleFontFamily == '') {
                if(commonConfig[myPStyleFontFamily][myCStyleFontStyle] == undefined){
                    logTxt.write("\n" +myCStyle.name+ ": font variant \'"+myCStyleFontStyle+"\' for the font \'"+myPStyleFontFamily+"\' is not defined in config");
                    noError = false;
                    }
                }
            else if (typeof(config.referencesModifiedCStyles)  != 'undefined' && config.referencesModifiedCStyles[myCStyle.name] != 'undefined'){
                if (config.referencesModifiedCStyles[myCStyle.name].fontFamily != myCStyleFontFamily && config.referencesModifiedCStyles[myCStyle.name].fontStyle != myCStyleFontStyle){
                    logTxt.write("\n" +myCStyle.name+ ": \'"+myCStyleFontFamily+"\' font family or \'"+myCStyleFontStyle+"\' font variant is mismatch with template config");
                    noError = false;
                    }
                }
            else{
                logTxt.write("\n" +myCStyle.name+ ": is not defined in template config");
                noError = false;
                }
            }  
        }
    }
//---------------------------------------------------------------------------------------------------------------------------------------------------------|
if(noError){
        logTxt.write("SUCCESS | " + currentUser + " | "+ todaysDate + ".");
    }else{
        logTxt.write("\n\n" + "ERROR | " + currentUser + " | "+ todaysDate+ ".");
    }
logTxt.close();// log file closed after validating
app.activeDocument.viewPreferences.rulerOrigin = ruleOrg; 
app.activeDocument.close(SaveOptions.no);// active Template closed automatically without saving
//========================================================================================|
//styleValidation - Validate both config value with template object
function styleValidation(styleNamel){
    var stylelist=commonConfig.styleValidation.styleName[styleNamel];
    var styleLength = stylelist.length;
    var title = 0;
    for (var stylename=0; stylename < styleLength; stylename++){//looping stylename
        if (!(myDoc[styleNamel].itemByName(stylelist[stylename]).isValid)){   
            missingStyle=stylelist[stylename];
            if(title == 0){
                logTxt.write("\n\n" +  "Missing" + " " +  styleNamel +" :" );
                title=3;
                noError = false;
            }
            logTxt.write( "\n" + missingStyle);
        }    
    }
}
//========================================================================================|
//Checking textFrames in all layers of Document Page
function masterSpread(layerName){
    var title = 0; var flag=true;
    var pages = myDoc.pages;
    var textFramesArray = new Array("FIRST_FRAME","VERSO","RECTO");
    for(var p=0; p < pages.length; p++){        
        if (!myDoc.layers.itemByName(layerName).textFrames.itemByName(textFramesArray[p]).isValid ){
            flag=false;
            if (title == 0){
                logTxt.write("\n\n"+"Missing textFrame in "+myDoc.layers.itemByName(layerName).name +" :");
                title =5;
                noError = false;
            }
            logTxt.write("\n"+textFramesArray[p]);               
        }
    }
}
//========================================================================================|
// Text  Frame Links
function textFrameLinks(){
title=0;
var frameLinks= new Array("FIRST_FRAME","VERSO","RECTO");
var firstTextFrame= myDoc.textFrames.itemByName(frameLinks[0]);           
    if(!(firstTextFrame.nextTextFrame.name == myDoc.textFrames.itemByName(frameLinks[1]).name)){
        if (title == 0){
            logTxt.write("\n\n"+"Text Frames are not Linked"); //Writing WRONG Document Page Count to txt file
            title =5;                
            noError = false;
        }
    logTxt.write("\n" + frameLinks[0]+" is not linked with "+frameLinks[1]);
    }
    else if (!(myDoc.textFrames.itemByName(frameLinks[1]).nextTextFrame.name == myDoc.textFrames.itemByName(frameLinks[2]).name)){
        if (title == 0){
            logTxt.write("\n\n"+"Text Frames are not Linked"); //Writing WRONG Document Page Count to txt file
            title =5;   
            noError = false;
        }
    logTxt.write("\n" + frameLinks[1]+" is not linked with "+frameLinks[2]);
    }    
}
//========================================================================================|
function masterSpreadTextframeCheck(layoutName){
    var myDoc = app.activeDocument;
    var errorTitle = true;
    var masterSpread = app.activeDocument.masterSpreads;
    var errorMsgTxtFrame = "\n\n"+"Missing textFrame in masterSpread "+layoutName +":";
    
     if(!myDoc.masterSpreads.item("A-TXT").pages[0].pageItems.itemByName("VERSO").isValid){
         if(errorTitle){
         logTxt.write(errorMsgTxtFrame);
         errorTitle = false;
         }
         logTxt.write("\n"+"A-TXT - VERSO"); 
         }
     if(!myDoc.masterSpreads.item("A-TXT").pages[1].pageItems.itemByName("RECTO").isValid){
         if(errorTitle){
           logTxt.write(errorMsgTxtFrame);
           errorTitle = false;
           }
           logTxt.write("\n"+"A-TXT - RECTO"); 
         }
     if(!myDoc.masterSpreads.item("B-CO").pages[0].pageItems.itemByName("FIRST_FRAME").isValid){
         if(errorTitle){
            logTxt.write(errorMsgTxtFrame);
            errorTitle = false;
           }
           logTxt.write("\n"+"B-CO - FIRST_FRAME"); 
         }
     if(!myDoc.masterSpreads.item("B-CO").pages[1].pageItems.itemByName("FIRST_FRAME").isValid){
         if(errorTitle){
            logTxt.write(errorMsgTxtFrame);
            errorTitle = false;
           }
           logTxt.write("\n"+"B-CO - FIRST_FRAME"); 
         }
     if(errorTitle){
         //pageColumnDetails(layoutName);    //Need to update code until this function invoked 
         }
    if(!errorTitle){//Global error log check
    noError = false;
    }
}
//========================================================================================|
//Description :  Validating pageColumnDetails for all layout
//Created On : (10-01-2019)
function  pageColumnDetails(layoutName){
    var myDoc = app.activeDocument;
    var pageColumnDetails = config.pageColumnDetails;
    var openerPageColumnDetails = pageColumnDetails[layoutName].openerPageColumnDetails;
    var openerPageColumnDetailsLength = openerPageColumnDetails.length;
    var pagColDetailsRept ={};
    var pagColDetailsReptFlag = true;
    var pagColDetailsValid = true;
    var columnDetails = pageColumnDetails[layoutName].columnDetails;
    var columnDetailsLength = columnDetails.length;
    var colDetailsRept ={};
    var colDetailsReptFlag = true;
    var colDetailsValid = true;
    var errorMsgOpenerPage = "\n\n" + '"'+layoutName+'"' + " openerPageColumnDetails";
    var errorMsgcolDetails = "\n\n" +'"'+layoutName+'"'  + " columnDetails";
        //openerPageColumnDetails
        for(var pagCol=0; pagCol < openerPageColumnDetailsLength; pagCol++){
            var pagColDetails = openerPageColumnDetails[pagCol];
             if(pagColDetailsReptFlag){
                pagColDetailsRept["width"] =  pagColDetails.width;
                pagColDetailsReptFlag = false;
             }
         if(pagCol > 0 ){
             if(pagColDetails.width !=pagColDetailsRept.width){
                  if(pagColDetailsValid){  
                      logTxt.write(errorMsgOpenerPage);
                     pagColDetailsValid = false;
                 }
              logTxt.write("\nwidth [" + pagCol + "] in config is incorrect");
              }
             pagColDetailsRept["gutter"] = pagColDetails.gutter;
             if(pagCol > 1){
                 if(pagColDetails.gutter != pagColDetailsRept.gutter){
                     if(pagColDetailsValid){
                         logTxt.write(errorMsgOpenerPage);
                        pagColDetailsValid = false;
                     }
                 logTxt.write("\ngutter [" + pagCol + "] in config is incorrect");
                 }
             }
         }
     }
        //columnDetails
        for(var colDet=0; colDet < columnDetailsLength; colDet++){
            var colDetails = columnDetails[colDet];
             if(colDetailsReptFlag){
                colDetailsRept["width"] =  colDetails.width;
                colDetailsReptFlag = false;
             }
         if(colDet > 0){
             if(colDetails.width !=colDetailsRept.width){
                  if(colDetailsValid){
                     logTxt.write(errorMsgcolDetails);
                     colDetailsValid =false;
                 }
                 logTxt.write("\nwidth [" + colDet + "] in config is incorrect");
             }
             colDetailsRept["gutter"] = colDetails.gutter;
             if(colDet > 1){
                 if(colDetails.gutter != colDetailsRept.gutter){
                      if(colDetailsValid){
                         logTxt.write(errorMsgcolDetails);
                         colDetailsValid =false;
                         }
                      logTxt.write("\ngutter [" + colDet + "] in config is incorrect");
                     }
                 }
             }
        }
    if(!pagColDetailsValid || !colDetailsValid){//Global error log check
        noError = false;
    }
    if(pagColDetailsValid && colDetailsValid){ //(B-CO && A-TXT)
    // check the config pageColumnDetails with masterSpreads
    var layTxtFrmV_0 = myDoc.masterSpreads.item("A-TXT").pages[0].textFrames.itemByName("VERSO");
    var layTxtFrmR_1 = myDoc.masterSpreads.item("A-TXT").pages[1].textFrames.itemByName("RECTO");
    var layTxtFrmFF_0 = myDoc.masterSpreads.item("B-CO").pages[0].textFrames.itemByName("FIRST_FRAME");
    var layTxtFrmFF_1 = myDoc.masterSpreads.item("B-CO").pages[1].textFrames.itemByName("FIRST_FRAME");
    var layoutTopic = '"'+layoutName+'"'+" pageColumnDetails \n"
    var errorMsgBco = "\n\n"+layoutTopic+"Masterpage B-CO";
    var bcoTitle = true;
    //openerPageMargin & otherPageMargin
    var openerPageMargin = pageColumnDetails[layoutName].openerPageMargin;
    var otherPageMargin = pageColumnDetails[layoutName].otherPageMargin; //pageColumnDetails
    //Template width |  gutter |  bounds values are stored in variable (B-CO)
    var bcoFF0Width = parseFloat(layTxtFrmFF_0.textFramePreferences.textColumnFixedWidth).toFixed(3);
    var bcoFF1Width = parseFloat(layTxtFrmFF_1.textFramePreferences.textColumnFixedWidth).toFixed(3);
    var bcoDocPage1FFWidth = parseFloat(myDoc.pages[0].textFrames.itemByName("FIRST_FRAME").textFramePreferences.textColumnFixedWidth).toFixed(3);
    
    var bcoFF0Gutter = parseFloat(layTxtFrmFF_0.textFramePreferences.textColumnGutter).toFixed(3);
    var bcoFF1Gutter = parseFloat(layTxtFrmFF_1.textFramePreferences.textColumnGutter).toFixed(3);
    var bcoDocPage1FFGutter = parseFloat(myDoc.pages[0].textFrames.itemByName("FIRST_FRAME").textFramePreferences.textColumnGutter).toFixed(3);
    
    var bcoFF0Outside = parseFloat(layTxtFrmFF_0.geometricBounds[1]).toFixed(3);
    var bcoFF1Inside = parseFloat(layTxtFrmFF_1.geometricBounds[1]).toFixed(3);
    var bcoDocPage1FFBounds = parseFloat(myDoc.pages[0].textFrames.itemByName("FIRST_FRAME").geometricBounds[1]).toFixed(3);
    //Template width |  gutter |  bounds values are stored in variable  (A-TXT)
    var atxtV0Width = parseFloat( layTxtFrmV_0.textFramePreferences.textColumnFixedWidth).toFixed(3);
    var atxtR1Width = parseFloat(layTxtFrmR_1.textFramePreferences.textColumnFixedWidth).toFixed(3);
    var atxtDocPage2V0Width = parseFloat(myDoc.pages[1].textFrames.itemByName("VERSO").textFramePreferences.textColumnFixedWidth).toFixed(3);
    var atxtDocPage3R1Width = parseFloat(myDoc.pages[2].textFrames.itemByName("RECTO").textFramePreferences.textColumnFixedWidth).toFixed(3);
    
    var atxtV0Gutter = parseFloat(layTxtFrmV_0.textFramePreferences.textColumnGutter).toFixed(3);
    var atxtR1Gutter = parseFloat(layTxtFrmR_1.textFramePreferences.textColumnGutter).toFixed(3);
    var atxtDocPage2V0Gutter = parseFloat(myDoc.pages[1].textFrames.itemByName("VERSO").textFramePreferences.textColumnGutter).toFixed(3);
    var atxtDocPage3R1Gutter = parseFloat(myDoc.pages[2].textFrames.itemByName("RECTO").textFramePreferences.textColumnGutter).toFixed(3);
    
    var atxtV0Outside = parseFloat(layTxtFrmV_0.geometricBounds[1]).toFixed(3);
    var atxtR1Inside = parseFloat(layTxtFrmR_1.geometricBounds[1]).toFixed(3);
    var atxtDocPage2V0Outside = parseFloat(myDoc.pages[1].textFrames.itemByName("VERSO").geometricBounds[1]).toFixed(3);
    var atxtDocPage3R1Inside = parseFloat(myDoc.pages[2].textFrames.itemByName("RECTO").geometricBounds[1]).toFixed(3);
    
       // check first_frame width (B-CO)
           if( bcoFF0Width != pagColDetailsRept.width){ //B-CO page 0 width
               if(bcoTitle){
                   logTxt.write(errorMsgBco);
                   layoutTopic = '';
                   bcoTitle =false;
                   }
               logTxt.write( "\npage 0: FIRST_FRAME width does not matches with config");
                logTxt.write(" [TEMPLATE: "+ bcoFF0Width+" | CONFIG: "+parseFloat(pagColDetailsRept.width)+" ]");
               }
           if( bcoFF1Width != pagColDetailsRept.width){ //B-CO page 1 width
               if(bcoTitle){
                   logTxt.write(errorMsgBco);
                   layoutTopic = '';
                   bcoTitle =false;
                    }
                logTxt.write("\npage 1: FIRST_FRAME width does not matches with config");
                 logTxt.write(" [TEMPLATE: "+ bcoFF1Width+" | CONFIG: "+parseFloat(pagColDetailsRept.width)+" ]");
               }
            // check document page 1 first_frame width (B-CO)
            if( bcoDocPage1FFWidth != pagColDetailsRept.width){
                 if(bcoTitle){
                       logTxt.write(errorMsgBco);
                       layoutTopic = '';
                       bcoTitle =false;
                    }
                 logTxt.write("\nDocument page 1: FIRST_FRAME width does not matches with config");
                  logTxt.write(" [TEMPLATE: "+bcoDocPage1FFWidth+" | CONFIG: "+parseFloat(pagColDetailsRept.width)+" ]");
            }
           // check first_frame gutter (B-CO)
           if(typeof(pagColDetailsRept.gutter) != 'undefined'){ // if there is more than two column text |  validate the gutten value between the column
               if( bcoFF0Gutter != pagColDetailsRept.gutter){ // B-CO page 0 gutter
                    if(bcoTitle){
                       logTxt.write(errorMsgBco);
                       layoutTopic = '';
                       bcoTitle =false;
                    }
                logTxt.write("\npage 0: FIRST_FRAME gutter does not matches with config");
                 logTxt.write(" [TEMPLATE: "+bcoFF0Gutter+" | CONFIG: "+ parseFloat(pagColDetailsRept.gutter)+" ]");
               }
               if( bcoFF1Gutter != pagColDetailsRept.gutter){ //B-CO page 1 gutter
                    if(bcoTitle){
                       logTxt.write(errorMsgBco);
                       layoutTopic = '';
                       bcoTitle =false;
                    }
                   logTxt.write("\npage 1: FIRST_FRAME gutter does not matches with config");
                    logTxt.write(" [TEMPLATE: "+bcoFF1Gutter+" | CONFIG: "+ parseFloat(pagColDetailsRept.gutter)+" ]");
               }
             // check document page first_frame gutter (B-CO)
                if( bcoDocPage1FFGutter != pagColDetailsRept.gutter ){
                      if(bcoTitle){
                       logTxt.write(errorMsgBco);
                       layoutTopic = '';
                       bcoTitle =false;
                    }
                    logTxt.write("\nDocument page 1: FIRST_FRAME gutter does not matches with config");
                     logTxt.write(" [TEMPLATE: "+bcoDocPage1FFGutter+" | CONFIG: "+parseFloat( pagColDetailsRept.gutter)+" ]");
                }
           }
        // check first_frame bounds (B-CO)
        if( bcoFF0Outside != openerPageMargin.outside){  //B-CO outside
             if(bcoTitle){
                       logTxt.write(errorMsgBco);
                       layoutTopic = '';
                       bcoTitle =false;
                    }
                logTxt.write("\npage 0: FIRST_FRAME X1 bound does not matches with config");
                 logTxt.write(" [TEMPLATE: "+bcoFF0Outside+" | CONFIG: "+parseFloat(openerPageMargin.outside)+" ]");
            }
        if( bcoFF1Inside != openerPageMargin.inside){  //B-CO
             if(bcoTitle){
                       logTxt.write(errorMsgBco);
                       layoutTopic = '';
                       bcoTitle =false;
                    }
                 logTxt.write("\npage 1: FIRST_FRAME X1 bound does not matches with config");
                   logTxt.write(" [TEMPLATE: "+bcoFF1Inside +" | CONFIG: "+parseFloat(openerPageMargin.inside)+" ]");
            }
        // check document page first_frame bounds (B-CO)
        if( bcoDocPage1FFBounds != openerPageMargin.inside){
             if(bcoTitle){
                       logTxt.write(errorMsgBco);
                       layoutTopic = '';
                       bcoTitle =false;
                    }
                 logTxt.write("\nDocument page 1: FIRST_FRAME X1 bound does not matches with config");
                 logTxt.write(" [TEMPLATE: "+bcoDocPage1FFBounds+" | CONFIG: "+parseFloat(openerPageMargin.inside)+" ]");
            }
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
        var errorMsgAtxt = "\n\n"+layoutTopic+"Masterpage A-TXT";
        var atxtTitle =true;
            // check verso & recto width (A-TXT)
           if( atxtV0Width != colDetailsRept.width){  //A-TXT page 0 width
               if(atxtTitle){
                   logTxt.write(errorMsgAtxt);
                   layoutTopic = '';
                   atxtTitle = false;
                   }
                logTxt.write("\npage 0: VERSO width does not matches with config");
                 logTxt.write(" [TEMPLATE: "+atxtV0Width+" | CONFIG: "+parseFloat(colDetailsRept.width)+" ]");
               }
           if( atxtR1Width != colDetailsRept.width){  //A-TXT page 1 width
               if(atxtTitle){
                   logTxt.write(errorMsgAtxt);
                   layoutTopic = '';
                   atxtTitle = false;
                   }
               logTxt.write("\npage 1: VERSO width does not matches with config");
                logTxt.write(" [TEMPLATE: "+atxtR1Width+" | CONFIG: "+parseFloat(colDetailsRept.width)+" ]");
               }
           // check document page 2 - verso & page 3 - recto width (A-TXT)
            if( atxtDocPage2V0Width != colDetailsRept.width){ //A-TXT doc page 2 width
                 if(atxtTitle){
                       logTxt.write(errorMsgAtxt);
                       layoutTopic = '';
                       atxtTitle =false;
                    }
                 logTxt.write("\nDocument page 2: VERSO width does not matches with config");
                 logTxt.write(" [TEMPLATE: "+atxtDocPage2V0Width+" | CONFIG: "+parseFloat(colDetailsRept.width)+" ]");
            }
            if( atxtDocPage3R1Width != colDetailsRept.width){ //A-TXT doc page 3 width
                 if(atxtTitle){
                       logTxt.write(errorMsgAtxt);
                       layoutTopic = '';
                       atxtTitle =false;
                    }
                 logTxt.write("\nDocument page 3: RECTO width does not matches with config");
                  logTxt.write(" [TEMPLATE: "+atxtDocPage3R1Width+" | CONFIG: "+parseFloat(colDetailsRept.width)+" ]");
            }
            // check verso & recto gutter (A-TXT)
           if(typeof(colDetailsRept.gutter) != 'undefined'){
               if( atxtV0Gutter != colDetailsRept.gutter){ // A-TXT page 0 gutter
                   if(atxtTitle){
                       logTxt.write(errorMsgAtxt);
                       layoutTopic = '';
                       atxtTitle = false;
                   }
                   logTxt.write("\npage 0: VERSO gutter does not matches with config");
                     logTxt.write(" [TEMPLATE: "+atxtV0Gutter+" | CONFIG: "+parseFloat(colDetailsRept.gutter)+" ]");
               }
               if( atxtR1Gutter != colDetailsRept.gutter){ // A-TXT page 1 gutter
                   if(atxtTitle){
                       logTxt.write(errorMsgAtxt);
                       layoutTopic = '';
                       atxtTitle = false;
                   }
                   logTxt.write("\npage 1: RECTO gutter does not matches with config");
                     logTxt.write(" [TEMPLATE: "+atxtR1Gutter+" | CONFIG: "+parseFloat( colDetailsRept.gutter)+" ]");
               }
           // check document page verso & recto gutter (A-TXT)
                if( atxtDocPage2V0Gutter != colDetailsRept.gutter ){
                      if(atxtTitle){
                       logTxt.write(errorMsgAtxt);
                       layoutTopic = '';
                       atxtTitle =false;
                    }
                    logTxt.write("\nDocument page 2: VERSO gutter does not matches with config");
                     logTxt.write(" [TEMPLATE: "+atxtDocPage2V0Gutter+" | CONFIG: "+parseFloat(colDetailsRept.gutter )+" ]");
                }
              if( atxtDocPage3R1Gutter != colDetailsRept.gutter ){
                      if(atxtTitle){
                       logTxt.write(errorMsgAtxt);
                       layoutTopic = '';
                       atxtTitle =false;
                    }
                    logTxt.write("\nDocument page 3: RECTO gutter does not matches with config");
                     logTxt.write( "[TEMPLATE: "+atxtDocPage3R1Gutter+" | CONFIG: "+parseFloat(colDetailsRept.gutter)+"]");
                }
           }
            // check verso & recto bound (A-TXT)
         if( atxtV0Outside != otherPageMargin.outside){ // A-TXT
               if(atxtTitle){
                       logTxt.write(errorMsgAtxt);
                       layoutTopic = '';
                       atxtTitle = false;
                   }
                logTxt.write("\npage 0: VERSO X1 bound does not matches with config");
                 logTxt.write(" [TEMPLATE: "+atxtV0Outside+" | CONFIG: "+parseFloat(otherPageMargin.outside)+" ]");
            }
        if( atxtR1Inside != otherPageMargin.inside){ // A-TXT
              if(atxtTitle){
                       logTxt.write(errorMsgAtxt);
                       layoutTopic = '';
                       atxtTitle = false;
                   }
                logTxt.write("\npage 1: RECTO X1 bound does not matches with config");
                 logTxt.write(" [TEMPLATE: "+atxtR1Inside+" | CONFIG: "+parseFloat(otherPageMargin.inside)+" ]");
          }
      // check document page verso & recto bounds (B-CO)
        if( atxtDocPage2V0Outside != otherPageMargin.outside){
             if(atxtTitle){
                   logTxt.write(errorMsgAtxt);
                   layoutTopic = '';
                   atxtTitle =false;
                }
             logTxt.write("\nDocument page 2: VERSO X1 bound does not matches with config");
              logTxt.write(" [TEMPLATE: "+atxtDocPage2V0Outside+" | CONFIG: "+parseFloat(otherPageMargin.outside)+" ]");
        }
        if( atxtDocPage3R1Inside != parseFloat(otherPageMargin.inside)){
             if(atxtTitle){
                   logTxt.write(errorMsgAtxt);
                   layoutTopic = '';
                   atxtTitle =false;
                }
             logTxt.write("\nDocument page 3: RECTO X1 bound does not matches with config");
              logTxt.write(" [TEMPLATE: "+atxtDocPage3R1Inside+" | CONFIG: "+parseFloat(otherPageMargin.inside)+" ]");
        }
        
   }// end if 
    if(!bcoTitle || !atxtTitle){//Global error log check
        noError = false;
    }
}// end function
//========================================================================================|
//Description :  Validate the style related to base style, we have three parameters. Details below:
//                          1. Style type paragraph | character | object 
//                          2. base style which is existing in template 
//                          3. Related style list from base style
//Created On : (15-July-2020)
//Developed by : aNTON
//---------------------------------------------------------------------------------------------------------------------------------------------------------|
function validationRelatedToBaseStyle(styleTypeName, baseStyle, validationStyles){
    var styleList = validationStyles.split("|");
    var ListStyleFlag = true;
    if(myDoc[styleTypeName].itemByName(baseStyle).isValid){
        for (var sl=0; sl < styleList.length; sl++){
            var currStyle = myDoc[styleTypeName].itemByName(styleList[sl]);
            if(!currStyle.isValid){
                if (ListStyleFlag){
                     logTxt.write("\n\n" +  "Missing" + " " +  styleTypeName +" based on existing style:");
                     ListStyleFlag = false;
                     }
                logTxt.write("\n"+styleList[sl]);
                noError = false;
                }
            }
        }
    }
//---------------------------------------------------------------------------------------------------------------------------------------------------------|